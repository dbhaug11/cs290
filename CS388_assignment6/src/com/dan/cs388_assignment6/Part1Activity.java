package com.dan.cs388_assignment6;

import android.os.Build;
import android.os.Bundle;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Part1Activity extends Activity {
	MyAnimationView animView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_part1);
		RelativeLayout container = (RelativeLayout) findViewById(R.id.container);
		animView = new MyAnimationView(this);
		container.addView(animView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.part1, menu);
		return true;
	}

	public void animateBall(View v) {
		animView.animateBall();
	}

	public class MyAnimationView extends View implements
			ValueAnimator.AnimatorUpdateListener {
		ShapeHolder ball;

		public MyAnimationView(Context context) {
			super(context);
		}

		public void animateBall() {
			int startX = 50;
			int startY = 150;
			int endX = 500;
			int endY = 500;
			OvalShape shape = new OvalShape();
			shape.resize(50, 50);
			ShapeDrawable drawable = new ShapeDrawable(shape);
			ball = new ShapeHolder(drawable);
			ball.setX(startX);
			ball.setY(startY);
			AnimatorSet s1 = new AnimatorSet();
			ValueAnimator animRight = ObjectAnimator.ofFloat(ball, "x", startX,
					endX);
			animRight.setInterpolator(new AccelerateInterpolator());
			animRight.setDuration(1000);
			animRight.addUpdateListener(this);
			ValueAnimator animDown = ObjectAnimator.ofFloat(ball, "y", startY,
					endY);
			animDown.setInterpolator(new DecelerateInterpolator());
			animDown.setDuration(1000);
			animDown.addUpdateListener(this);
			ValueAnimator animLeft = ObjectAnimator.ofFloat(ball, "x", endX,
					startX);
			animLeft.setInterpolator(new AccelerateInterpolator());
			animLeft.setDuration(1000);
			animLeft.addUpdateListener(this);
			ValueAnimator animUp = ObjectAnimator.ofFloat(ball, "y", endY,
					startY);
			animUp.setInterpolator(new DecelerateInterpolator());
			animUp.setDuration(1000);
			animUp.addUpdateListener(this);
			s1.playSequentially(animRight, animDown, animLeft, animUp);
			s1.start();
			invalidate();
		}

		public void onDraw(Canvas canvas) {
			if (ball != null) {
				canvas.save();
				System.out.println("ball is at X:" + getX() + " Y:" + getY());
				canvas.translate(ball.getX(), ball.getY());
				ball.getShape().draw(canvas);
				canvas.restore();
			}
		}

		public void onAnimationUpdate(ValueAnimator animation) {
			invalidate();
		}
	}
}
