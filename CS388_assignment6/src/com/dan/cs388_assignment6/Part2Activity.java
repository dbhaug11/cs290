package com.dan.cs388_assignment6;

import android.os.Build;
import android.os.Bundle;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.Menu;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.RelativeLayout;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Part2Activity extends Activity {
	MyAnimationView animView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_part2);
		RelativeLayout container = (RelativeLayout) findViewById(R.id.container2);
		animView = new MyAnimationView(this);
		container.addView(animView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.part2, menu);
		return true;
	}
	
	public void animateBall(View v) {
		animView.bounceBall();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public class MyAnimationView extends View implements
			ValueAnimator.AnimatorUpdateListener {
		ShapeHolder ball;
		
		public MyAnimationView(Context context) {
			super(context);
			
		}
		
		void bounceBall(){
			int startX = 50;
			int startY = 150;
			int endY = 1000;
			OvalShape shape = new OvalShape();
			shape.resize(50, 50);
			ShapeDrawable drawable = new ShapeDrawable(shape);
			ball = new ShapeHolder(drawable);
			ball.setX(startX);
			ball.setY(startY);
			ValueAnimator anim= ObjectAnimator.ofFloat(ball, "y", startY,
					endY);
			anim.setInterpolator(new BounceInterpolator());
			anim.setDuration(2000);
			anim.addUpdateListener(this);
			anim.start();
			
		}

		@Override
		public void onAnimationUpdate(ValueAnimator animation) {
			invalidate();

		}
		public void onDraw(Canvas canvas) {
			if (ball != null) {
				canvas.save();
				System.out.println("ball is at X:" + getX() + " Y:" + getY());
				canvas.translate(ball.getX(), ball.getY());
				ball.getShape().draw(canvas);
				canvas.restore();
			}
		}

	}

}
