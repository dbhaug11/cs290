package com.dan.cs388_assignment6;

import android.os.Build;
import android.os.Bundle;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.view.Menu;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.LinearLayout;

public class Part3Activity extends Activity {
	MyAnimationView animView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_part3);
		LinearLayout container = (LinearLayout) findViewById(R.id.container3);
		animView = new MyAnimationView(this);
		container.addView(animView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.part2, menu);
		return true;
	}

	public void animateTextView(View v) {
		animView.flipCard(findViewById(R.id.nameView));
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public class MyAnimationView extends View implements
			AnimatorListener {
		View target;
		public MyAnimationView(Context context) {
			super(context);
		}

		void flipCard(View v) {
			target=v;
			ViewPropertyAnimator anim1 = v.animate().setDuration(2000).rotationY(720).setListener(this);
			anim1.start();
			
		}

		@Override
		public void onAnimationCancel(Animator animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			target.animate().setDuration(250).alpha(0).start();
		}

		@Override
		public void onAnimationRepeat(Animator animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationStart(Animator animation) {
			// TODO Auto-generated method stub
			
		}
	}
}
