package com.example.android.geofence;

import java.io.IOException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static Context context;
	public final static String DB_NAME = "geofence";
	public static final int SCHEMA_VERSION = 1;
	private static final String CREATE_GEOFENCES_TABLE_SQL = "CREATE TABLE "
			+ GeofenceContract.Geofences.TABLE_NAME + " ("
			+ GeofenceContract.Geofences._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ GeofenceContract.Geofences.NAME + " TEXT,"
			+ GeofenceContract.Geofences.LATITUDE + " TEXT,"
			+ GeofenceContract.Geofences.LONGITUDE + " TEXT,"
			+ GeofenceContract.Geofences.RADIUS + " TEXT,"
			+ GeofenceContract.Geofences.EXPIRATION_DURATION + " INTEGER,"
			+ GeofenceContract.Geofences.TRANSITION_TYPE + " INTEGER" + ");";

	private static DatabaseHelper instance = null;

	private DatabaseHelper(Context context) {
		super(context, DB_NAME, null, SCHEMA_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.beginTransaction();
			db.execSQL(CREATE_GEOFENCES_TABLE_SQL);
			db.setTransactionSuccessful();
		} catch (Exception e) {
			Log.v("Courses", "DB Creation failed...");
			e.printStackTrace(System.err);
		} finally {
			db.endTransaction();
		}
		filldatabase(db);
	}

	private void filldatabase(SQLiteDatabase db) {
		SimpleGeofence[] fences = null;
		try {
			fences = JsonFileReader.getLocationsFromFile(context.getAssets()
					.open("locations.json"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (fences != null) {
			for (int i = 0; i < fences.length; i++) {
				ContentValues values = new ContentValues();
				values.put(GeofenceContract.Geofences.NAME, fences[i].getName());
				values.put(GeofenceContract.Geofences.LATITUDE,
						fences[i].getLatitude());
				values.put(GeofenceContract.Geofences.LONGITUDE,
						fences[i].getLongitude());
				values.put(GeofenceContract.Geofences.RADIUS,
						fences[i].getRadius());
				values.put(GeofenceContract.Geofences.EXPIRATION_DURATION,
						fences[i].getExpirationDuration());
				values.put(GeofenceContract.Geofences.TRANSITION_TYPE,
						fences[i].getTransitionType());
				db.insert(GeofenceContract.Geofences.TABLE_NAME,
						GeofenceContract.Geofences.NAME, values);
			}
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "
				+ GeofenceContract.Geofences.TABLE_NAME);
		this.onCreate(db);
	}

	public static DatabaseHelper instance(Activity activity) {

		if (instance == null) {
			instance = new DatabaseHelper(activity.getApplicationContext());
		}
		return instance;
	}

	public static DatabaseHelper instance(Context context) {
		if (instance == null) {
			instance = new DatabaseHelper(context);
		}
		return instance;
	}

}
