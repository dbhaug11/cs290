package com.example.android.geofence;

import android.net.Uri;
import android.provider.BaseColumns;

public final class GeofenceContract {
	private GeofenceContract() {
	}

	public static final String AUTHORITY = "com.example.android.geofence.geofenceProvider";
	public static final String[] DETAILS_PROJECTION = { Geofences._ID,
			Geofences.NAME, Geofences.LATITUDE, Geofences.LONGITUDE,
			Geofences.RADIUS, Geofences.EXPIRATION_DURATION,
			Geofences.TRANSITION_TYPE };
	public static final String[] GEOFENCE_PROJECTION = { Geofences._ID,
			Geofences.NAME, Geofences.LATITUDE, Geofences.LONGITUDE };

	/* Inner class that defines the table contents */
	public static abstract class Geofences implements BaseColumns {
		public static final String TABLE_NAME = "geofences";
		public static final String NAME = "name";
		public static final String LATITUDE = "latitude";
		public static final String LONGITUDE = "longitude";
		public static final String RADIUS = "radius";
		public static final String EXPIRATION_DURATION = "expirationDuration";
		public static final String TRANSITION_TYPE = "transitionType";
		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ AUTHORITY + "/geofences");
	}
}
