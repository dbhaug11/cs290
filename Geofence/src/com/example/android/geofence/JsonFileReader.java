package com.example.android.geofence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.location.Geofence;

public class JsonFileReader {
	static SimpleGeofence[] getLocationsFromFile(InputStream is) {
		Reader reader = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(reader);
		ArrayList<SimpleGeofence> list = new ArrayList<SimpleGeofence>();
		try {
			JSONArray jArr = new JSONArray(br.readLine());
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject jobj = jArr.getJSONObject(i);
				SimpleGeofence loc = new SimpleGeofence(jobj.getString("id"),
						jobj.getDouble("latitude"),
						jobj.getDouble("longitude"),
						((float)jobj.getDouble("radius")),
						jobj.getLong("expiration_duration"), Geofence.GEOFENCE_TRANSITION_ENTER, jobj.getString("name"));
				list.add(loc);
			}
		} catch (IOException e) {

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleGeofence[] locs = new SimpleGeofence[list.size()];
		for (int i = 0; i < locs.length; i++) {
			locs[i] = list.get(i);
		}
		return locs;
	}
}
