package com.commonsware.android.lifecycle;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Intent;


public class ThirdActivity extends LifecycleLoggingActivity implements OnClickListener {
	public static final String EXTRA_MESSAGE="msg";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.third);

	}
	public void reportResult(View v){
		setResult(RESULT_OK,getIntent());
		finish();
	}
	@Override
	public void onClick(View arg0) {
		Log.d(getClass().getSimpleName(),"clicked");
	}

}
