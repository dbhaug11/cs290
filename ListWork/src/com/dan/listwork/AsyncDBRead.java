package com.dan.listwork;

import java.util.Locale;

import com.dan.listwork.CoursePagerActivity.DCPagerAdapter;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FilterQueryProvider;
import android.widget.TextView;
import android.widget.SimpleCursorAdapter.ViewBinder;

public class AsyncDBRead extends AsyncTask<Activity, Void, Cursor> {
	private CoursePagerActivity activity;
	private DCPagerAdapter pagerAdapter;
	private ViewPager pager;

	@Override
	protected Cursor doInBackground(Activity... activity) {
		this.activity = (CoursePagerActivity) activity[0];
		return this.activity.getContentResolver()
				.query(CourseContract.Courses.CONTENT_URI,
						CourseContract.LISTVIEW_PROJECTION, null,
						new String[] {}, null);
	}

	@Override
	protected void onPostExecute(Cursor cursor) {
		final CustomIconCursorAdapter adapter = new CustomIconCursorAdapter(
				activity, R.layout.row, cursor,
				new String[] { CourseContract.Courses.NUMBER },
				new int[] { R.id.text1 }, 0);
		ViewBinder vb = new CustomViewBinder();
		adapter.setViewBinder(vb);
		adapter.setFilterQueryProvider(new FilterQueryProvider() {
			@Override
			public Cursor runQuery(CharSequence arg0) {
				String selection = CourseContract.Courses.DEPARTMENT + " =?";
				String[] selectionArgs = new String[] { arg0.toString()
						.toUpperCase(Locale.US) };
				if (arg0.equals("")) {
					selection = null;
					selectionArgs = new String[] {};
				}
				return activity.getContentResolver().query(
						CourseContract.Courses.CONTENT_URI,
						CourseContract.LISTVIEW_PROJECTION, selection,
						selectionArgs, null);
			}

		});
		FragmentManager fm = activity.getFragmentManager();
		pagerAdapter = activity.new DCPagerAdapter(fm);
		pagerAdapter.setAdapter(adapter);
		pager = (ViewPager) activity.findViewById(R.id.pager);
		pager.setAdapter(pagerAdapter);

		final ActionBar actionBar = activity.getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (int i = 0; i < pagerAdapter.getCount(); i++) {
			final ActionBar.Tab tab = actionBar.newTab().setText(
					pagerAdapter.favorites[i]);
			tab.setTabListener(new ActionBar.TabListener() {

				@Override
				public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
				}

				@Override
				public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
					pager.setCurrentItem(tab.getPosition());
				}

				@Override
				public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
				}

			});
			actionBar.addTab(tab);
			pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
				@Override
				public void onPageSelected(int currentPosition) {
					adapter.getFilter().filter(
							pagerAdapter.favorites[currentPosition]);
					actionBar.setSelectedNavigationItem(currentPosition);
				}
			});
		}
	}

	class CustomViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
			if (view.getId() == R.id.text1) {
				((TextView) view)
						.setText(cursor.getString(cursor
								.getColumnIndex(CourseContract.Courses.DEPARTMENT))
								+ cursor.getString(columnIndex)
								+ cursor.getString(cursor
										.getColumnIndex(CourseContract.Courses.SECTION)));
				return true;
			}
			return false;
		}
	}

}
