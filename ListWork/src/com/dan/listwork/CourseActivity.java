package com.dan.listwork;

import java.util.Locale;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TextView;

public class CourseActivity extends ListActivity {
	private ActionMode actionMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course);
	}

	@Override
	public void onResume() {
		super.onResume();
		AsyncTask<Activity, Integer, Cursor> task = new AsyncDatabaseRead();
		task.execute(this);
		getListView().setLongClickable(true);
		getListView().setOnItemLongClickListener(new OnLongClickListener());
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		if (this.actionMode != null) {
			checkBox(v, position);
			return;
		}

		Intent intent = new Intent(this, DetailsActivity.class);
		TextView tv = (TextView) v.findViewById(R.id.text1);
		intent.putExtra(getResources().getString(R.string.extras_id), tv
				.getText().toString());
		intent.putExtra(getResources().getString(R.string.positionInArray),
				position);
		intent.putExtra(getResources().getString(R.string.extras_id), id);
		startActivity(intent);
	}

	public void checkBox(View view, int position) {
		CheckedTextView check = (CheckedTextView) view.findViewById(R.id.text1);
		if (check != null) {
			check.toggle();
		}
		getListView().setItemChecked(position, check.isChecked());
		actionMode.invalidate();
		if (getListView().getCheckedItemCount() < 1) {
			actionMode.finish();
		}
	}

	protected void deleteItems() {
		SparseBooleanArray arr = getListView().getCheckedItemPositions();
		Long[] args = new Long[arr.size()];
		int count = 0;
		for (int i = 0; i < getListView().getChildCount(); i++) {
			if (arr.get(i)) {
				CheckedTextView temp = (CheckedTextView) getListView()
						.getChildAt(arr.keyAt(count)).findViewById(R.id.text1);
				args[count] = getListView().getItemIdAtPosition(i);
				count++;
				temp.toggle();
			}
		}
		AsyncTask<Long, Void, Cursor> task = new AsyncDatabaseDelete();
		task.execute(args);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case (R.id.create):

			// rkg -1 explicit intent?
			Intent intent = new Intent(this, EditCourseActivity.class);
			startActivity(intent);

		}
		return true;
	}

	class CourseListItem {
		public CourseListItem(String courseNumber, int drawableId) {
			this.courseNumber = courseNumber;
			this.drawableId = drawableId;
		}

		public String toString() {
			return courseNumber;
		}

		String courseNumber;
		int drawableId;
	}

	class OnLongClickListener implements OnItemLongClickListener {
		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			if (actionMode != null) {
				return false;
			}
			actionMode = startActionMode(actionModeCallback);
			checkBox(arg1, arg2);
			arg1.setSelected(true);
			return true;
		}
	}

	private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.contextual, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			MenuItem item = menu.findItem(R.id.edit);
			System.out.println("in prepare "
					+ getListView().getCheckedItemCount());
			if (getListView().getCheckedItemCount() == 1) {
				System.out.println("set true");
				item.setEnabled(true);
			} else {
				System.out.println("set false");
				item.setEnabled(false);
			}
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.delete:
				deleteItems();
				mode.finish();
				return true;
			case R.id.edit:
				Intent intent = new Intent(CourseActivity.this,
						EditCourseActivity.class);
				intent.putExtra(getResources().getString(R.string.extras_id),
						getListView().getCheckedItemIds()[0]);
				startActivity(intent);
				mode.finish();
				return true;
			default:
				return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			actionMode = null;
		}

	};

	class AsyncDatabaseRead extends AsyncTask<Activity, Integer, Cursor> {
		Activity activity;
		private final String PROGRESS_MESSAGE = "Loading Courses...";
		ProgressDialog progressBar;

		@Override
		protected Cursor doInBackground(Activity... activity) {
			this.activity = activity[0];
			takeALongTime();
			return getContentResolver().query(
					CourseContract.Courses.CONTENT_URI,
					CourseContract.LISTVIEW_PROJECTION, null, new String[] {},
					null);
		}

		@Override
		protected void onPostExecute(Cursor cursor) {
			progressBar.hide();
			CustomIconCursorAdapter adapter = new CustomIconCursorAdapter(
					CourseActivity.this, R.layout.row, cursor,
					new String[] { CourseContract.Courses.NUMBER },
					new int[] { R.id.text1 }, 0);
			ViewBinder vb = new CustomViewBinder();
			adapter.setViewBinder(vb);
			adapter.setFilterQueryProvider(new FilterQueryProvider() {
				@Override
				public Cursor runQuery(CharSequence arg0) {
					String selection = CourseContract.Courses.DEPARTMENT
							+ " =?";
					String[] selectionArgs = new String[] { arg0.toString()
							.toUpperCase(Locale.US) };
					if (arg0.equals("")) {
						selection = null;
						selectionArgs = new String[] {};
					}
					return getContentResolver().query(
							CourseContract.Courses.CONTENT_URI,
							CourseContract.LISTVIEW_PROJECTION, selection,
							selectionArgs, null);
				}

			});
			setListAdapter(adapter);
		}

		private void takeALongTime() {
			int time = 0;
			while (time < 1000) {
				try {
					Thread.sleep(100);
					publishProgress((int) (time / 1000.0 * 100));
				} catch (InterruptedException e) {

				}
				time = time + 100;
			}
		}

		@Override
		protected void onPreExecute() {
			progressBar = new ProgressDialog(CourseActivity.this);
			progressBar.setCancelable(true);
			progressBar.setMessage(PROGRESS_MESSAGE);
			progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressBar.setProgress(0);
			progressBar.setMax(100);
			progressBar.show();
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			progressBar.setProgress(progress[0]);
		}
	}

	class AsyncDatabaseDelete extends AsyncTask<Long, Void, Cursor> {

		@Override
		protected Cursor doInBackground(Long... arr) {
			DatabaseHelper db = DatabaseHelper.instance(CourseActivity.this);
			for (int i = 0; i < arr.length; i++) {
				getContentResolver().delete(
						ContentUris.withAppendedId(
								CourseContract.Courses.CONTENT_URI, arr[i]),
						CourseContract.Courses._ID + " = ?",
						new String[] { String.valueOf(arr[i]) });

			}
			return db.getReadableDatabase().query(
					CourseContract.Courses.TABLE_NAME,
					CourseContract.LISTVIEW_PROJECTION, null, new String[] {},
					null, null, null, null);
		}

		@Override
		protected void onPostExecute(Cursor cursor) {
			((SimpleCursorAdapter) getListAdapter()).changeCursor(cursor);
		}
	}

	class CustomViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
			if (view.getId() == R.id.text1) {
				((TextView) view)
						.setText(cursor.getString(cursor
								.getColumnIndex(CourseContract.Courses.DEPARTMENT))
								+ cursor.getString(columnIndex)
								+ cursor.getString(cursor
										.getColumnIndex(CourseContract.Courses.SECTION)));
				return true;
			}
			return false;
		}
	}
}
