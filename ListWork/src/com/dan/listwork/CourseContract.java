package com.dan.listwork;

import android.net.Uri;
import android.provider.BaseColumns;

public final class CourseContract {
	private CourseContract() {
	}

	public static final String AUTHORITY = "com.dan.listwork.courseProvider";

	public static final String[] LISTVIEW_PROJECTION = { Courses._ID,
			Courses.DEPARTMENT, Courses.NUMBER, Courses.SECTION };
	public static final String[] DETAILS_PROJECTION = { Courses._ID,
			Courses.TITLE, Courses.NUMBER, Courses.INSTRUCTOR,
			Courses.MEETINGTIMES, Courses.DESCRIPTION, Courses.DAYS,
			Courses.SECTION, Courses.DEPARTMENT };

	/* Inner class that defines the table contents */
	public static abstract class Courses implements BaseColumns {
		public static final String TABLE_NAME = "courses";
		public static final String TITLE = "title";
		public static final String NUMBER = "number";
		public static final String INSTRUCTOR = "instructor";
		public static final String MEETINGTIMES = "meetingTimes";
		public static final String DESCRIPTION = "description";
		public static final String DAYS = "days";
		public static final String SECTION = "section";
		public static final String DEPARTMENT = "department";
		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ AUTHORITY + "/courses");
		public static final String CONTENT_TYPE = null;
		public static final String CONTENT_ITEM_TYPE = null;
	}
}
