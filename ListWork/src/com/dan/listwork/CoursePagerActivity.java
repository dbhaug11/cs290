package com.dan.listwork;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CursorAdapter;
import android.support.v4.app.FragmentActivity;

public class CoursePagerActivity extends FragmentActivity {
	private SharedPreferences prefs;
	
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setContentView(R.layout.activity_course_pager);
		AsyncDBRead dbTask = new AsyncDBRead();
		dbTask.execute(this);
		prefs=PreferenceManager.getDefaultSharedPreferences(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.course_pager, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			startActivity(new Intent(this, EditPreferences.class));
			
		}
		return true;
	}

	public class DCPagerAdapter extends FragmentStatePagerAdapter {
		Activity activity;
		
		final String[] departments = { "AC", "AR", "B", "BU", "C", "CIMB",
				"CJ", "CO", "COM", "CS", "DA", "E", "EC", "ED", "ELB", "ES",
				"F", "FN", "FS", "GE", "GK", "GS", "H", "HB", "HS", "ID", "L",
				"LCT", "LH", "LOND", "M", "MG", "MK", "MR", "MT", "MU", "MUE",
				"MUL", "P", "PD", "PE", "PH", "PS", "PY", "S", "SP", "ST",
				"STI", "TA", "TAX", "TH", "THEO" };
		
		String[] favorites;
		private CursorAdapter cursorAdapter;

		public DCPagerAdapter(FragmentManager fm) {
			super(fm);
			Set<String> favoriteset=prefs.getStringSet("departmentList", null);
			if(favoriteset!=null){
				favorites=new String[favoriteset.size()];
				Iterator it=favoriteset.iterator();
				for(int i=0;i<favoriteset.size();i++){
					favorites[i]=(String) it.next();
				}
			}else{
				favorites=departments;
			}
		}

		public void setAdapter(CursorAdapter adapter) {
			this.cursorAdapter = adapter;
			adapter.getFilter().filter(favorites[0]);
		}

		@Override
		public Fragment getItem(int i) {
			DepartmentList fragment = new DepartmentList();
			Bundle args = new Bundle();
			args.putString("object", favorites[i]);
			fragment.setArguments(args);
			fragment.setAdapter(cursorAdapter);
			return fragment;
		}

		@Override
		public int getCount() {
			return favorites.length;
		}
	}

}
