package com.dan.listwork;

import java.util.ArrayList;

import com.dan.listwork.CourseActivity.CourseListItem;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter<T> extends BaseAdapter {

	private ArrayList<T> objects;
	private Context context;
	private int resource;

	public CustomAdapter(Context context, int resource, T[] objects) {
		this.objects = new ArrayList<T>();
		for (int i = 0; i < objects.length; i++) {
			this.objects.add(objects[i]);
		}
		this.context = context;
		this.resource = resource;

	}

	@Override
	public int getCount() {
		return objects.size();
	}


	@Override
	public T getItem(int arg0) {
		return (T)objects.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vg;
		if (convertView == null) {
			vg = ((Activity) context).getLayoutInflater().inflate(resource,
					parent, false);
		} else {
			vg = convertView;
		}
		TextView tv = (TextView) vg.findViewById(R.id.text1); 
		tv.setText((CharSequence) getItem(position).toString());
		ImageView iv = (ImageView) vg.findViewById(R.id.icon);
		iv.setImageDrawable(context.getResources().getDrawable(
				((CourseListItem) getItem(position)).drawableId));
		return vg;
	}

// This does not work when inserting items
	@Override
	public void notifyDataSetChanged() {
		DetailsStore detailsStore = DetailsStore.instance();
		for (int i = getCount()-1; i>=0; i--) {
			if (detailsStore.getDetails(getItem(i).toString()) == null) {
				objects.remove(i);
			}
		}
		super.notifyDataSetChanged();
	}
}
