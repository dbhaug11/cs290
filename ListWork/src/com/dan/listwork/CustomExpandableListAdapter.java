package com.dan.listwork;

import java.text.DecimalFormat;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
	private List<Instructor> instructors;
	private Context context;
	public CustomExpandableListAdapter(Context context, List<Instructor> instructors){
		super();
		this.context=context;
		this.instructors=instructors;
	}

	@Override
	public Object getChild(int groupPostion, int childPosition) {
		return instructors.get(groupPostion).getRating(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return instructors.get(groupPosition).getRating(childPosition).getID();
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		if(convertView==null){
			 convertView=((Activity) context).getLayoutInflater().inflate(R.layout.child_layout, null);
		}
		((TextView) convertView.
				findViewById(R.id.child)).setText(instructors.get(groupPosition).getRating(childPosition).getCourseNumber()+" ");
		((TextView) convertView.findViewById(R.id.child_rating)).setText(String.valueOf(instructors.get(groupPosition).getRating(childPosition).getRating()));
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return instructors.get(groupPosition).getNumberOfCourses();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return instructors.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return instructors.size();
		
	}

	@Override
	public long getGroupId(int groupPosition) {
		return instructors.get(groupPosition).getId();
		
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		DecimalFormat df = new DecimalFormat("#.00");
		
		if(convertView==null){
			 convertView=((Activity) context).getLayoutInflater().inflate(R.layout.group_layout, null);
		}
		((TextView) convertView.findViewById(R.id.group)).setText(instructors.get(groupPosition).getName()+" ");
		((TextView) convertView.findViewById(R.id.average)).setText(df.format(instructors.get(groupPosition).getAverage()));
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
