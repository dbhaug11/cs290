package com.dan.listwork;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;

public class CustomIconCursorAdapter extends SimpleCursorAdapter {
	private Context context;

	public CustomIconCursorAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to, int flags) {
		super(context, layout, c, from, to, flags);
		this.context=context;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view=super.getView(position, convertView, parent);
		ImageView iv=(ImageView) view.findViewById(R.id.icon);
		iv.setImageDrawable(context.getResources().getDrawable(position%2==0?R.drawable.book1:R.drawable.book2));
		return view;
		
	}

}
