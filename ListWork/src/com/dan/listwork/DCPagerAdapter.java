package com.dan.listwork;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;

public class DCPagerAdapter extends FragmentStatePagerAdapter {
	Activity activity;
	static String[] departments = { "AC", "AR", "B", "BU", "C", "CIMB", "CJ",
			"CO", "COM", "CS", "DA", "E", "EC", "ED", "ELB", "ES", "F", "FN",
			"FS", "GE", "GK", "GS", "H", "HB", "HS", "ID", "L", "LCT", "LH",
			"LOND", "M", "MG", "MK", "MR", "MT", "MU", "MUE", "MUL", "P", "PD",
			"PE", "PH", "PS", "PY", "S", "SP", "ST", "STI", "TA", "TAX", "TH",
			"THEO" };

	public DCPagerAdapter(FragmentManager fm) {
		super(fm);
		AsyncDBRead dbTask = new AsyncDBRead();

	}

	@Override
	public Fragment getItem(int i) {
		DepartmentList fragment = new DepartmentList();

		Bundle args= new Bundle();
		args.putString(fragment.ARG_OBJECT,departments[i]);
		return fragment;
	}

	@Override
	public int getCount() {
		return 52;
	}

}
