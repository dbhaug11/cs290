package com.dan.listwork;

import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import au.com.bytecode.opencsv.CSVReader;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static Context context;
	public final static String DB_NAME = "courses";
	public static final int SCHEMA_VERSION = 3;
	private static final String CREATE_COURSES_TABLE_SQL = "CREATE TABLE "
			+ CourseContract.Courses.TABLE_NAME + " ("
			+ CourseContract.Courses._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ CourseContract.Courses.TITLE + " TEXT,"
			+ CourseContract.Courses.NUMBER + " TEXT,"
			+ CourseContract.Courses.INSTRUCTOR + " TEXT,"
			+ CourseContract.Courses.MEETINGTIMES + " TEXT,"
			+ CourseContract.Courses.DESCRIPTION + " TEXT,"
			+ CourseContract.Courses.DAYS + " TEXT,"
			+ CourseContract.Courses.DEPARTMENT + " TEXT,"
			+ CourseContract.Courses.SECTION + " TEXT" + ");";

	private static DatabaseHelper instance = null;

	private DatabaseHelper(Context context) {
		super(context, DB_NAME, null, SCHEMA_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.beginTransaction();
			db.execSQL(CREATE_COURSES_TABLE_SQL);
			db.setTransactionSuccessful();
		} catch (Exception e) {
			Log.v("Courses", "DB Creation failed...");
			e.printStackTrace(System.err);
		} finally {
			db.endTransaction();
		}
		fillDatabase(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + CourseContract.Courses.TABLE_NAME);
		this.onCreate(db);
	}

	public static DatabaseHelper instance(Activity activity) {

		if (instance == null) {
			instance = new DatabaseHelper(activity.getApplicationContext());
		}
		return instance;
	}

	public static DatabaseHelper instance(Context context) {
		if (instance == null) {
			instance = new DatabaseHelper(context);
		}
		return instance;
	}

	private void fillDatabase(SQLiteDatabase db) {
		AssetManager am = context.getAssets();

		try {
			InputStream csv = am.open("srcrsect.csv");
			CSVReader reader = new CSVReader(new InputStreamReader(csv));
			String[] line;
			while ((line = reader.readNext()) != null) {
				ContentValues values = new ContentValues();
				values.put(CourseContract.Courses.DEPARTMENT, line[1]);
				values.put(CourseContract.Courses.NUMBER, line[2]);
				values.put(CourseContract.Courses.SECTION, line[3]);
				values.put(CourseContract.Courses.TITLE, line[4]);
				values.put(CourseContract.Courses.DESCRIPTION, line[4]);
				values.put(CourseContract.Courses.INSTRUCTOR, line[5]);
				values.put(CourseContract.Courses.DAYS, "MTWRF");
				values.put(CourseContract.Courses.MEETINGTIMES, "8-5");
				db.insert(CourseContract.Courses.TABLE_NAME,
						CourseContract.Courses.DAYS, values);
			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
