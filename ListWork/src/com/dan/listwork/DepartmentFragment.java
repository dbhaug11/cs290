package com.dan.listwork;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;

public class DepartmentFragment extends Fragment implements TextWatcher {
	ListActivity activity;

	public View onCreateView(LayoutInflater in, ViewGroup c, Bundle b) {
		View v= in.inflate(R.layout.fragment_for_dept, c, false);
		EditText text=(EditText) v.findViewById(R.id.filter);
		text.addTextChangedListener(this);

		return v;
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		this.activity = (ListActivity) activity;
		super.onAttach(activity);
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		SimpleCursorAdapter adapter = (SimpleCursorAdapter) activity
				.getListAdapter();
		adapter.getFilter().filter(s);
	}
}
