package com.dan.listwork;

import android.app.ListFragment;
import android.widget.CursorAdapter;

public class DepartmentList extends ListFragment {
	public final String ARG_OBJECT = "object";
	
	public void setAdapter(CursorAdapter adapter) {
		setListAdapter(adapter);
	}
}
