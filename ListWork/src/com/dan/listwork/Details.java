package com.dan.listwork;

public class Details {
	private String courseNumber;
	private String courseTitle;
	private String courseDays;
	private String courseInstructor;
	private String courseDescription;
	private String courseMeetingTimes;
	
	public Details(){
		courseNumber="";
		courseTitle="";
		courseDays="";
		courseInstructor="";
		courseDescription="";
		courseMeetingTimes="";
	}
	public Details(String courseNumber ,String courseTitle,String courseDays,String courseInstructor,String courseDescription,String courseMeetingTimes){
		this.courseNumber=courseNumber;
		this.courseTitle=courseTitle;
		this.courseDays=courseDays;
		this.courseInstructor=courseInstructor;
		this.courseDescription=courseDescription;
		this.courseMeetingTimes=courseMeetingTimes;
	}
	public String getCourseNumber() {
		return courseNumber;
	}
	protected void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}
	protected void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}
	protected void setCourseDays(String courseDays) {
		this.courseDays = courseDays;
	}
	protected void setCourseInstructor(String courseInstructor) {
		this.courseInstructor = courseInstructor;
	}
	protected void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}
	protected void setCourseMeetingTimes(String courseMeetingTimes) {
		this.courseMeetingTimes = courseMeetingTimes;
	}
	public String getCourseTitle() {
		return courseTitle;
	}
	public String getCourseDays() {
		return courseDays;
	}
	public String getCourseInstructor() {
		return courseInstructor;
	}
	public String getCourseDescription() {
		return courseDescription;
	}
	public String getCourseMeetingTimes() {
		return courseMeetingTimes;
	}
}