package com.dan.listwork;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class DetailsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
	}

	@Override
	public void onResume() {
		super.onResume();
		AsyncTask<Long, Integer, Cursor> task = new asyncDatabaseTask();
		task.execute(getIntent().getLongExtra(
				getResources().getString(R.string.extras_id), 0));
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.details, menu);
		return true;
	}

	public void goBack(View v) {
		this.finish();
	}

	private void fillViews(Cursor c) {
		if (c != null) {
			c.moveToFirst();
			TextView temp = (TextView) this.findViewById(R.id.courseTitle);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.TITLE)));
			temp = (TextView) this.findViewById(R.id.courseNumber);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.DEPARTMENT))
					+ c.getString(c
							.getColumnIndex(CourseContract.Courses.NUMBER))
					+ c.getString(c
							.getColumnIndex(CourseContract.Courses.SECTION)));
			temp = (TextView) this.findViewById(R.id.courseInstructor);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.INSTRUCTOR)));
			temp = (TextView) this.findViewById(R.id.courseMeetingTime);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.MEETINGTIMES)));
			temp = (TextView) this.findViewById(R.id.courseDescription);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.DESCRIPTION)));
			temp = (TextView) this.findViewById(R.id.courseDays);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.DAYS)));
		}
	}

	class asyncDatabaseTask extends AsyncTask<Long, Integer, Cursor> {

		@Override
		protected Cursor doInBackground(Long... params) {
			return getContentResolver().query(
					ContentUris.withAppendedId(
							CourseContract.Courses.CONTENT_URI, params[0]),
					CourseContract.DETAILS_PROJECTION, null, null, null);
		}

		@Override
		protected void onPostExecute(Cursor cursor) {
			fillViews(cursor);
			cursor.close();
		}
	}
}