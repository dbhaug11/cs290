package com.dan.listwork;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

// Remove these extraneous imports

import com.dan.listwork.details.CS220;
import com.dan.listwork.details.CS290;
import com.dan.listwork.details.CS415;
import com.dan.listwork.details.P304;
import com.dan.listwork.details.ST232;

public class DetailsStore {

	private static final DetailsStore instance = new DetailsStore();
	private static final Map<String, Details> courseMap = new HashMap<String, Details>() {
		{
			put("CS290",
					new Details(
							"CS290",
							"Android Programming", 
							"MWF",
							"Gordon", 
							"A course about Android Programming",
							"8:35-9:45"));
			put("CS220", 
					new Details(
							"CS220", 
							"Discrete Mathematics", 
							"MWF",
							"Weiner",
							"A course about Discrete Mathematics used in Computing",
							"2:55-3:45"));
			put("CS415",
					new Details(
							"CS415",
							"Operating Systems",
							"MWF",
							"Gordon",
							"A course about operating systems and the hardware they run on",
							"1:30-2:45"));
			put("ST232", 
					new Details(
							"ST232", 
							"Statistics", 
							"TR", 
							"Dennis",
							"A course that covers basic statistics",
							"8:10-9"));
			put("P304", 
					new Details(
							"P304", 
							"Intro to Modern Physics", 
							"MTW",
							"Cho",
							"An introduction to Modern Physics: everything since 1905",
							"9:45-11:00 MW, 12:45-3:45 T"));
		}
	};

	private DetailsStore() {

	}

	public static DetailsStore instance() {
		return instance;
	}

	public Details getDetails(String course) {
		return courseMap.get(course);
	}

	public void removeDetails(String course) {
		courseMap.remove(course);
	}


// rkg -1 Why Object[] 
// They are Details.
	public Object[] getArray() {
		Collection courses = courseMap.values();
		return courses.toArray();
	}
}
