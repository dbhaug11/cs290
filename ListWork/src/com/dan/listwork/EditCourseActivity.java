package com.dan.listwork;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class EditCourseActivity extends Activity {
	private static final long NO_ID = -1;
	private Long id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_course);
		id = getIntent().getLongExtra("course", -1);
		if (isUpdate()) {
			AsyncDatabaseRead task = new AsyncDatabaseRead();
			task.execute(id);
		}
	}
	private void fillViews(Cursor c) {
		if (c != null) {
			c.moveToFirst();
			EditText temp = (EditText) findViewById(R.id.CourseTitleField);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.TITLE)));
			temp = (EditText) findViewById(R.id.CourseNumberField);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.NUMBER)));
			temp = (EditText) findViewById(R.id.CourseInstructorField);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.INSTRUCTOR)));
			temp = (EditText) findViewById(R.id.CourseMeetingTimesField);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.MEETINGTIMES)));
			temp = (EditText) findViewById(R.id.CourseDescriptionField);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.DESCRIPTION)));
			temp = (EditText) findViewById(R.id.CourseDaysField);
			temp.setText(c.getString(c
					.getColumnIndex(CourseContract.Courses.DAYS)));
		}
		TextView tv = (TextView) findViewById(R.id.TextViewTitle);
		tv.setText(R.string.updateCourse);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_course, menu);
		MenuItem mi = menu.findItem(R.id.updateOrCreate);
		mi.setTitle(isUpdate() ? getResources().getString(R.string.updateLabel)
				: getResources().getString(R.string.createLabel));
		mi.setVisible(true);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuitem) {
		switch (menuitem.getItemId()) {
		case R.id.cancel:

			break;
		case R.id.updateOrCreate:
			ContentValues values = new ContentValues();
			values.put(CourseContract.Courses.TITLE,
					((EditText) findViewById(R.id.CourseTitleField)).getText()
							.toString());
			values.put(CourseContract.Courses.NUMBER,
					((EditText) findViewById(R.id.CourseNumberField)).getText()
							.toString());
			values.put(CourseContract.Courses.INSTRUCTOR,
					((EditText) findViewById(R.id.CourseInstructorField))
							.getText().toString());
			values.put(CourseContract.Courses.MEETINGTIMES,
					((EditText) findViewById(R.id.CourseMeetingTimesField))
							.getText().toString());
			values.put(CourseContract.Courses.DAYS,
					((EditText) findViewById(R.id.CourseDaysField)).getText()
							.toString());
			values.put(CourseContract.Courses.DESCRIPTION,
					((EditText) findViewById(R.id.CourseDescriptionField))
							.getText().toString());

			if (isUpdate()) {
				getContentResolver().update(
						ContentUris.withAppendedId(
								CourseContract.Courses.CONTENT_URI, id),
						values, CourseContract.Courses._ID + "= ?",
						new String[] { String.valueOf(id) });
			} else {
				id = (long) 0;
				getContentResolver()
						.insert(ContentUris.withAppendedId(
								CourseContract.Courses.CONTENT_URI, id), values);
			}
			break;

		default:
			return false;
		}
		finish();
		return true;
	}

	private boolean isUpdate() {
		return id != NO_ID;
	}

	class AsyncDatabaseRead extends AsyncTask<Long, Integer, Cursor> {

		@Override
		protected Cursor doInBackground(Long... arg0) {
			return getContentResolver().query(
					ContentUris.withAppendedId(
							CourseContract.Courses.CONTENT_URI, id),
					CourseContract.DETAILS_PROJECTION,
					CourseContract.Courses._ID + " = ?",
					new String[] { String.valueOf(id) }, null);
		}
		@Override
		protected void onPostExecute(Cursor cursor){
			fillViews(cursor);
			cursor.close();
		}
	}
}	
