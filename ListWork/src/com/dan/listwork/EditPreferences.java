package com.dan.listwork;

import java.util.List;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Menu;

public class EditPreferences extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_preferences, menu);
		return true;
	}
	
	public void onBuildHeaders(List<Header> target){
		loadHeadersFromResource(R.xml.preference_headers,target);
	}

}
