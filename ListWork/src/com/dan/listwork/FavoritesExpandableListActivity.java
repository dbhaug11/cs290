package com.dan.listwork;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class FavoritesExpandableListActivity extends ExpandableListActivity {
	private List<HashMap<String, String>> groups;
	private List<List<HashMap<String, String>>> children;

	@SuppressWarnings("serial")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		groups = new ArrayList<HashMap<String, String>>();

		groups.add(new HashMap<String, String>() {
			{
				put("col0", "AC/DC ");
			}
		});
		groups.add(new HashMap<String, String>() {
			{
				put("col0", "Foo Fighters ");
			}
		});
		groups.add(new HashMap<String, String>() {
			{
				put("col0", "Shinedown ");
			}
		});

		children = new ArrayList<List<HashMap<String, String>>>();
		List<HashMap<String, String>> ACDCGroup = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> backInBlack = new HashMap<String, String>() {
			{
				put("col0", "Back in Black ");
				put("col1", "9");
			}
		};
		ACDCGroup.add(backInBlack);
		HashMap<String, String> highway = new HashMap<String, String>() {
			{
				put("col0", "Highway To Hell ");
				put("col1", "9.3");
			}
		};
		ACDCGroup.add(highway);
		children.add(ACDCGroup);
		List<HashMap<String, String>> fooFightersGroup = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> walk = new HashMap<String, String>() {
			{
				put("col0", "Walk ");
				put("col1", "9.8");
			}
		};
		fooFightersGroup.add(walk);
		HashMap<String, String> pretender = new HashMap<String, String>() {
			{
				put("col0", "The Pretender ");
				put("col1", "9.5");
			}
		};
		fooFightersGroup.add(pretender);
		HashMap<String, String> doa = new HashMap<String, String>() {
			{
				put("col0", "DOA ");
				put("col1", "9");
			}
		};
		fooFightersGroup.add(doa);
		HashMap<String, String> wheels = new HashMap<String, String>() {
			{
				put("col0", "Wheels ");
				put("col1", "9.1");
			}
		};
		fooFightersGroup.add(wheels);
		children.add(fooFightersGroup);

		List<HashMap<String, String>> shinedownGroup = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> soundOfMadness = new HashMap<String, String>() {
			{
				put("col0", "Sound of Madness ");
				put("col1", "9.3");
			}
		};
		shinedownGroup.add(soundOfMadness);
		HashMap<String, String> enemies = new HashMap<String, String>() {
			{
				put("col0", "Enemies ");
				put("col1", "9");
			}
		};
		shinedownGroup.add(enemies);
		HashMap<String, String> throughTheGhost = new HashMap<String, String>() {
			{
				put("col0", "Through the Ghost ");
				put("col1", "8.5");
			}
		};
		shinedownGroup.add(throughTheGhost);
		children.add(shinedownGroup);

		setListAdapter(new CustomExpandableListAdapter(this, groups,
				R.layout.group_layout, new String[] { "col0", "col1" },
				new int[] { R.id.group, R.id.average }, children,
				R.layout.child_layout, new String[] { "col0", "col1" },
				new int[] { R.id.child, R.id.child_rating }));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.expandable_list, menu);
		return true;
	}

	public void onGroupExpand(int groupPosition) {
		super.onGroupExpand(groupPosition);
		ExpandableListView elv = (ExpandableListView) findViewById(android.R.id.list);
		for (int i = 0; i < groups.size(); i++) {
			if (elv.isGroupExpanded(i) && i != groupPosition) {
				elv.collapseGroup(i);
			}
		}
	}

	class CustomExpandableListAdapter extends SimpleExpandableListAdapter {

		public CustomExpandableListAdapter(Context context,
				List<? extends Map<String, ?>> groupData, int groupLayout,
				String[] groupFrom, int[] groupTo,
				List<? extends List<? extends Map<String, ?>>> childData,
				int childLayout, String[] childFrom, int[] childTo) {

			super(context, groupData, groupLayout, groupFrom, groupTo,
					childData, childLayout, childFrom, childTo);
		}

		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			View view = super.getGroupView(groupPosition, isExpanded,
					convertView, parent);
			float avg = 0;
			for (int i = 0; i < children.get(groupPosition).size(); i++) {
				avg += Float.valueOf(children.get(groupPosition).get(i)
						.get("col1"));
			}
			avg = avg / children.get(groupPosition).size();
			DecimalFormat df = new DecimalFormat("#.00");
			df.format(avg);
			((TextView) view.findViewById(R.id.average))
					.setText(df.format(avg));
			view.setBackgroundColor(groupPosition % 2 == 0 ? Color.LTGRAY
					: Color.WHITE);
			return view;
		}

	}

}
