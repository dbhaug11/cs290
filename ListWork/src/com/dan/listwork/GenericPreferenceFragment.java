package com.dan.listwork;

import android.os.Bundle;
import android.preference.PreferenceFragment;

public class GenericPreferenceFragment extends PreferenceFragment {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int res = getActivity().getResources().getIdentifier(
				getArguments().getString("resource"), "xml",
				getActivity().getPackageName());
		addPreferencesFromResource(res);
	}
}
