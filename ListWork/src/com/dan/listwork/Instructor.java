package com.dan.listwork;

import java.util.ArrayList;
import java.util.List;

public class Instructor {
	private String name;
	private List<Rating> ratings;
	private int id;
	public Instructor(String name, int id){
		this.name=name;
		this.id=id;
		ratings=new ArrayList<Rating>();
	}
	
	public String getName(){
		return name;
	}
	public void addRating(Rating rating){
		ratings.add(rating);
		
	}
	public Rating getRating(int index){
		return ratings.get(index);
	}
	
	public List<Rating> getRatings(){
		return ratings;
	}
	
	public int getNumberOfCourses(){
		return ratings.size();
	}
	
	public float getAverage(){
		float avg=0;
		for(int i=0;i<ratings.size();i++){
			avg+=ratings.get(i).getRating();
		}
		return avg/ratings.size();
	}
	public int getId(){
		return id;
	}
}
