package com.dan.listwork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONReader {
	public static List<Instructor> readRatings(InputStream is) {
		Reader reader = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(reader);
		ArrayList<Instructor> list = new ArrayList<Instructor>();
		JSONArray jArr = null;
		try {
			jArr = new JSONArray(br.readLine());
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject jobj = jArr.getJSONObject(i);
				Instructor inst = new Instructor(jobj.getString("name"),jobj.getInt("id"));
				JSONArray rankings = jobj.getJSONArray("rankings");
				int length = rankings.length();
				
				for (int j = 0; j < length; j++) {
					JSONObject jo = rankings.getJSONObject(j);
					String number = jo.getString("course");
					int id = jo.getInt("id");
					int instId = jo.getInt("instructor_id");
					int ranking = jo.getInt("value");
					inst.addRating(new Rating(number, ranking, id, instId));
				}
				list.add(inst);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return (List<Instructor>) list;
	}
}
