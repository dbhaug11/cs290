package com.dan.listwork;

public class Rating {
	private String courseNumber;
	private int rating;
	private int id;
	private int instructorID;
	public Rating(String courseNumber, int rating, int id, int instructorID){
		this.courseNumber=courseNumber;
		this.rating=rating;
		this.id=id;
		this.instructorID=instructorID;
	}
	
	public int getRating(){
		return rating;
	}
	
	public String getCourseNumber(){
		return courseNumber;
	}
	
	public int getID(){
		return id;
	}
	
	public int getInstructorID(){
		return instructorID;
	}
}
