package com.dan.listwork;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ExpandableListActivity;
import android.view.Menu;

public class RatingsListActivity extends ExpandableListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String resourceLoc = getIntent().getStringExtra(
				getResources().getString(R.string.extras_id));
		AsyncTask<String, ?, ?> task = new AsyncJSONRead();
		task.execute(resourceLoc);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ratings_list, menu);
		return true;
	}

	class AsyncJSONRead extends AsyncTask<String, Integer, List<Instructor>> {

		@Override
		protected List<Instructor> doInBackground(String... arg0) {
			List<Instructor> ratings = null;
			try {
				URL url = new URL(arg0[0]);
				HttpURLConnection urlConnection = (HttpURLConnection) url
						.openConnection();
				InputStream is = new BufferedInputStream(
						urlConnection.getInputStream());
				ratings = JSONReader.readRatings(is);
				
			} catch (MalformedURLException e) {
				try {
					ratings = JSONReader.readRatings(RatingsListActivity.this.getAssets().open(arg0[0]));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return ratings;
		}

		protected void onPostExecute(List<Instructor> list) {
			RatingsListActivity.this
					.setListAdapter(new CustomExpandableListAdapter(
							RatingsListActivity.this, list));
		}

	}

}
