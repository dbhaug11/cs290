package com.dan.listwork.details;

import com.dan.listwork.Details;

public class CS220 extends Details {
	public CS220(){
		setCourseNumber("CS220");
		setCourseTitle("Discrete Mathematics");
		setCourseInstructor("Weiner");
		setCourseDays("MWF");
		setCourseMeetingTimes("2:55-3:45");
		setCourseDescription("A course about Discrete Mathematics used in Computing");
	}
}
