package com.dan.listwork.details;

import com.dan.listwork.Details;

public class CS290 extends Details {
	public CS290(){
		setCourseNumber("CS290");
		setCourseTitle("Android Programming");
		setCourseInstructor("Gordon");
		setCourseDays("MWF");
		setCourseMeetingTimes("8:45-9:35");
		setCourseDescription("A course about Android Programming");
	}
}
