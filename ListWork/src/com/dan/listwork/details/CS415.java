package com.dan.listwork.details;

import com.dan.listwork.Details;

public class CS415 extends Details {
	public CS415(){
		setCourseNumber("CS415");
		setCourseTitle("Operating Systems");
		setCourseInstructor("Gordon");
		setCourseDays("MWF");
		setCourseMeetingTimes("1:30-2:45");
		setCourseDescription("A course about operating systems and the hardware they run on");
	}
}
