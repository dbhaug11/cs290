package com.dan.listwork.details;

import com.dan.listwork.Details;

public class P304 extends Details {
	public P304(){
		setCourseNumber("P304");
		setCourseTitle("Intro to Modern Pyhysics");
		setCourseInstructor("Cho");
		setCourseDays("MWF");
		setCourseMeetingTimes("9:45-11:00");
		setCourseDescription("An introduction to Modern Physics: everything since 1905");
	}
}
