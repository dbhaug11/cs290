package com.dan.listwork.details;

import com.dan.listwork.Details;

public class ST232 extends Details {
	public ST232(){
		setCourseNumber("ST232");
		setCourseTitle("Statistics");
		setCourseInstructor("Dennis");
		setCourseDays("TR");
		setCourseMeetingTimes("8:10-9:00");
		setCourseDescription("A course that covers basic statistics");
	}
}
