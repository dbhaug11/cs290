package com.dan.listwork.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.dan.listwork.Instructor;
import com.dan.listwork.JSONReader;

public class TestRatingRead {
	
	private static final int EXPECTED_SIZE = 155;
	private static List<Instructor> ratings;
	@Before
	public void setup() {
		try {
			ratings=JSONReader.readRatings(new FileInputStream(new File("instructors.json")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// You will have to write this method in order to initialize ratings
		// variable referenced below. Presumably, this is the result of your
		// read method. It is expected that each member of the list created
		// by read has a getName and getNumberOfCourses method.
	}

	@Test
	public void testLength() {
		assertEquals(EXPECTED_SIZE, ratings.size());
	}
	
	@Test 
	public void testFirst() {
		assertEquals(ratings.get(0).getName(), "AERSY");
	}
	
	@Test
	public void testFirstNumberOfCourses() {
		assertEquals(ratings.get(0).getNumberOfCourses(), 6);
	}
	
	@Test 
	public void testLast() {
		assertEquals(ratings.get(EXPECTED_SIZE-1).getName(), "AIIKLZ");
	}
	
	@Test
	public void testLastNumberOfCourses() {
		assertEquals(ratings.get(EXPECTED_SIZE-1).getNumberOfCourses(), 2);
	}

}
