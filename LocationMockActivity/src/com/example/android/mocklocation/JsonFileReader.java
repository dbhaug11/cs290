package com.example.android.mocklocation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonFileReader {
	static TestLocation[] getLocationsFromFile(InputStream is) {
		Reader reader = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(reader);
		ArrayList<TestLocation> list = new ArrayList<TestLocation>();
		try {
			JSONArray jArr = new JSONArray(br.readLine());
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject jobj = jArr.getJSONObject(i);
				TestLocation loc = new TestLocation(jobj.getString("id"),
						jobj.getDouble("latitude"),
						jobj.getDouble("longitude"),
						(float) jobj.getDouble("accuracy"));
				list.add(loc);
			}
		} catch (IOException e) {

		}
		TestLocation[] locs=new TestLocation[list.size()];
		for(int i=0;i<locs.length;i++){
			locs[i]=list.get(i);
		}
		return locs;
	}
}
