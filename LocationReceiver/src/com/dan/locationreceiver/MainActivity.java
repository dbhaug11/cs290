package com.dan.locationreceiver;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener,
		LocationListener{
	
	LocationRequest mLocationRequest;
	LocationClient mLocationClient;
	static final long UPDATE_INTERVAL=3000;
	static final long FASTEST_UPDATE_INTERVAL=1000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mLocationRequest=LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		mLocationRequest.setInterval(FASTEST_UPDATE_INTERVAL);
		mLocationClient=new LocationClient(this,this,this);

	}
	
	@Override
	protected void onStart(){
		super.onStart();
		mLocationClient.connect();
	}
	
	@Override
	protected void onStop(){
		super.onStart();
		if(mLocationClient.isConnected()){
			mLocationClient.removeLocationUpdates(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "failed to connect to location services", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle arg0) {
		Toast.makeText(this, "connected to location services", Toast.LENGTH_SHORT).show();
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
		
	}

	@Override
	public void onDisconnected() {
		Toast.makeText(this, "disconnected from location services", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onLocationChanged(Location arg0) {
		TextView tv=(TextView) findViewById(R.id.location);
		tv.setText("Latitude: "+arg0.getLatitude()+", Longitude: "+arg0.getLongitude());
		
	}

}
