package com.dan.multiscreenapp;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class CourseContentProvider extends android.content.ContentProvider {
	private static final int COURSES = 0;
	private static final int COURSES_ID = 1;
	private DatabaseHelper db;

	@Override
	public int delete(Uri uri, String arg1, String[] arg2) {
		return db.getReadableDatabase().delete(CourseContract.Courses.TABLE_NAME, arg1, arg2);
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		long id = db.getReadableDatabase().insert(CourseContract.Courses.TABLE_NAME, null, arg1);
		return ContentUris.withAppendedId(CourseContract.Courses.CONTENT_URI,
				id);
	}

	@Override
	public boolean onCreate() {
		db = DatabaseHelper.instance(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selections,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(CourseContract.Courses.TABLE_NAME);
		switch (sUriMatcher.match(uri)) {
		case COURSES:
			return db.getReadableDatabase().query(CourseContract.Courses.TABLE_NAME, projection,
					selections, selectionArgs, null, null, null);
		case COURSES_ID:
			Long id = ContentUris.parseId(uri);
			return db.getReadableDatabase()
					.query(CourseContract.Courses.TABLE_NAME,
							CourseContract.DETAILS_PROJECTION,
							CourseContract.Courses._ID + " = ?",
							new String[] { String.valueOf(id) }, null, null,
							null, null);
		default:
			throw new IllegalArgumentException("Unknown URI" + uri);
		}
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		return db.getReadableDatabase().update(CourseContract.Courses.TABLE_NAME, arg1, arg2, arg3);
	}

	private static final UriMatcher sUriMatcher;
	static {
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(CourseContract.AUTHORITY, "courses", COURSES);
		sUriMatcher.addURI(CourseContract.AUTHORITY, "courses/#", COURSES_ID);
	}
}
