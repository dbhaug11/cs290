package com.dan.multiscreenapp;

import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dan.multiscreenapp.dummy.DummyContent;

/**
 * A fragment representing a single Course detail screen. This fragment is
 * either contained in a {@link CourseListActivity} in two-pane mode (on
 * tablets) or a {@link CourseDetailActivity} on handsets.
 */
public class CourseDetailFragment extends Fragment implements
		LoaderCallbacks<Cursor> {

	private static final int URL_LOADER = 0;
	private View root;
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private Long mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public CourseDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = Long.valueOf(getArguments().getString(
					ARG_ITEM_ID));
		}
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(URL_LOADER, null,
				(android.app.LoaderManager.LoaderCallbacks<Cursor>) this);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_course_detail,
				container, false);
		if(getArguments().containsKey(getResources().getString(R.string.isTwoPane))){
			rootView.findViewById(R.id.doneButton).setVisibility(View.INVISIBLE);
		}
		root=rootView;
		return rootView;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle arg1) {
		switch (id) {
		case URL_LOADER:
			return new CursorLoader(getActivity().getApplicationContext(),
					CourseContract.Courses.CONTENT_URI,
					CourseContract.DETAILS_PROJECTION, null, null, null);
		default:
			return null;
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		if (arg1 != null) {
			arg1.moveToFirst();
			TextView temp = (TextView) root.findViewById(R.id.courseTitle);
			temp.setText(arg1.getString(arg1
					.getColumnIndex(CourseContract.Courses.TITLE)));
			temp = (TextView) root.findViewById(R.id.courseNumber);
			temp.setText(arg1.getString(arg1
					.getColumnIndex(CourseContract.Courses.DEPARTMENT))
					+ arg1.getString(arg1
							.getColumnIndex(CourseContract.Courses.NUMBER))
					+ arg1.getString(arg1
							.getColumnIndex(CourseContract.Courses.SECTION)));
			temp = (TextView) root.findViewById(R.id.courseInstructor);
			temp.setText(arg1.getString(arg1
					.getColumnIndex(CourseContract.Courses.INSTRUCTOR)));
			temp = (TextView) root.findViewById(R.id.courseMeetingTime);
			temp.setText(arg1.getString(arg1
					.getColumnIndex(CourseContract.Courses.MEETINGTIMES)));
			temp = (TextView) root.findViewById(R.id.courseDescription);
			temp.setText(arg1.getString(arg1
					.getColumnIndex(CourseContract.Courses.DESCRIPTION)));
			temp = (TextView) root.findViewById(R.id.courseDays);
			temp.setText(arg1.getString(arg1
					.getColumnIndex(CourseContract.Courses.DAYS)));
		}

	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub

	}
}
