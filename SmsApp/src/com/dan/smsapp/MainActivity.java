package com.dan.smsapp;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.provider.ContactsContract;

public class MainActivity extends Activity {

	private String phoneNumber = null;
	private int PICK_CONTACT = 10;
	private ListView lv;
	private ArrayList<String> textHistory;
	private ArrayAdapter<String> aa;
	private AsyncTask<Intent, Void, Cursor> task;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textHistory = new ArrayList<String>();
		lv = (ListView) findViewById(R.id.history);
		aa = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, textHistory);
		lv.setAdapter(aa);
		IntentFilter filter = new IntentFilter(getResources().getString(
				R.string.custom_intent));
		registerReceiver(new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent intent) {
				textHistory.add(intent.getStringExtra(getResources().getString(
						R.string.get_msg)));
				aa.notifyDataSetChanged();
			}

		}, filter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void getContact(View view) {
		Intent intent = new Intent(Intent.ACTION_PICK,
				ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, PICK_CONTACT);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {
			task = new AsyncContactLookup();
			task.execute(data);
		}
	}

	public void sendSMS(View view) {
		EditText et=(EditText) findViewById(R.id.editText);
		String message = et.getText()
				.toString();
		try {
			task.get(2000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Message Failed!", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		} catch (ExecutionException e) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Message Failed!", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		} catch (TimeoutException e) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Message Failed!", Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}
		if (message.equals("")) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Must have a message!", Toast.LENGTH_SHORT);
			toast.show();
			return;
		}
		if (phoneNumber == null) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Must pick a contact first!", Toast.LENGTH_SHORT);
			toast.show();
			return;
		}
		SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(phoneNumber, null, message, null, null);
		addToHistory(message, phoneNumber);
		et.setText("");
	}

	private void addToHistory(String text, String number) {
		textHistory.add(text + ", " + number);
		aa.notifyDataSetChanged();
	}

	public class AsyncContactLookup extends AsyncTask<Intent, Void, Cursor> {

		@Override
		protected Cursor doInBackground(Intent... intents) {
			Uri contactData = intents[0].getData();
			Cursor c = getContentResolver().query(contactData, null, null,
					null, null);
			if (c.moveToFirst()) {
				String id = c.getString(c
						.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

				String hasPhone = c
						.getString(c
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

				if (hasPhone.equalsIgnoreCase("1")) {
					Cursor phones = getContentResolver().query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { id }, null);
					return phones;
				} else {
					Toast toast = Toast.makeText(getApplicationContext(),
							"Contact doesn't have a phone number!",
							Toast.LENGTH_SHORT);
					toast.show();
				}
			}
			c.close();
			return null;
		}

		protected void onPostExecute(Cursor phones) {
			phones.moveToFirst();
			phoneNumber = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			phones.close();
		}

	}
}