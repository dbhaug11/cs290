package com.dan.smsapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		SmsMessage[] msgs = null;
		if (bundle != null) {
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];
			for (int i = 0; i < msgs.length; i++) {
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				Intent in = new Intent(context.getResources().getString(
						R.string.custom_intent));
				in.putExtra(
						context.getResources().getString(R.string.get_msg),
						msgs[i].getDisplayOriginatingAddress() + ", "
								+ msgs[i].getDisplayMessageBody());
				context.sendBroadcast(in);
			}
		}
	}
}
