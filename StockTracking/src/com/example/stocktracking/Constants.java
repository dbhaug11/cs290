package com.example.stocktracking;


// rkg
// Nice consolidation of constants
public class Constants {
	public static final String BASE_URL = "https://etws.etrade.com";
	public static final String SANDBOX_URL="https://etwssandbox.etrade.com";
	public static final String CONSUMER_KEY = "8305d032d15a072abee1655aca7b95e7";
	public static final String CONSUMER_SECRET = "876d21234a75d9d14caebc434ee030ba";
	public static final String REQUEST_URL = BASE_URL + "/oauth/request_token";
	public static final String REVOKE_URL= BASE_URL+"/oauth/revoke_access_token";
	
	public static final String SCOPE = null;
	public static final String ENCODING = "UTF-8";
	
	public static final String ACCESS_URL = BASE_URL + "/oauth/access_token";
	public static final String AUTHORIZE_URL = "https://us.etrade.com/e/t/etws/authorize?key="+CONSUMER_KEY;
	public static final String REVOKE_ACCESS_URL= BASE_URL+"/oauth/revoke_access_token";
	
	public static final String OAUTH_CALLBACK_SCHEME = "x-oauthflow";
	public static final String OAUTH_CALLBACK_HOST = "callback";
	public static final String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;
	
	public static final String MARKET_URL=SANDBOX_URL+"/market/sandbox/rest";
	public static final String PRODUCT_LOOKUP=MARKET_URL+"/productlookup/";
	public static final String QUOTE_URL=MARKET_URL+"/quote/";

}
