package com.example.stocktracking;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomStockListAdapter extends BaseAdapter {
	private Context context;
	private int resource;
	private Stock[] objects;

	public CustomStockListAdapter(Context context, int resource,
			Stock[] objects) {
		this.context = context;
		this.resource = resource;
		this.objects = objects;
	}

	@Override
	public int getCount() {
		return objects == null ? 0 : objects.length;
	}

	@Override
	public Object getItem(int arg0) {
		return objects == null ? null : objects[arg0];
	}

	@Override
	public long getItemId(int arg0) {
		if (objects != null) {
			return arg0 > objects.length ? -1 : arg0;
		}
		return -1;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vg;
		if (convertView == null) {
			vg = ((Activity) context).getLayoutInflater().inflate(resource,
					parent, false);
		} else {
			vg = convertView;
		}

		TextView tv = (TextView) vg.findViewById(R.id.stock_symbol_search);
		tv.setText(objects[position].getSymbol());
		tv = (TextView) vg.findViewById(R.id.company_name_search);
		tv.setText(objects[position].getName());
		Log.e("debug","end of getView");
		return vg;
	}

}
