package com.example.stocktracking;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static Context context;
	public final static String DB_NAME = "stocks";
	public static final int SCHEMA_VERSION = 6;
	private static final String CREATE_COURSES_TABLE_SQL = "CREATE TABLE "
			+ StockContract.Stocks.TABLE_NAME + " (" + StockContract.Stocks._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ StockContract.Stocks.NAME + " TEXT,"
			+ StockContract.Stocks.SYMBOL + " TEXT,"
			+ StockContract.Stocks.STARTING_PRICE + " TEXT,"
			+ StockContract.Stocks.DAYS_PRICE_CHANGE + " TEXT,"
			+ StockContract.Stocks.DATE_STARTED + " TEXT,"
			+ StockContract.Stocks.EPS + " TEXT,"
			+ StockContract.Stocks.EST_EARNINGS + " TEXT,"
			+ StockContract.Stocks.HIGH_52 + " TEXT,"
			+ StockContract.Stocks.LOW_52 + " TEXT,"
			+ StockContract.Stocks.LAST_TRADE + " TEXT,"
			+ StockContract.Stocks.VOLUME_10_DAY + " TEXT,"
			+ StockContract.Stocks.LAST_DATE_CHECKED + " INTEGER" + ")";

	private static DatabaseHelper instance = null;

	private DatabaseHelper(Context context) {
		super(context, DB_NAME, null, SCHEMA_VERSION);
		DatabaseHelper.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.beginTransaction();
			db.execSQL(CREATE_COURSES_TABLE_SQL);
			db.setTransactionSuccessful();
		} catch (Exception e) {
			Log.v("Courses", "DB Creation failed...");
			e.printStackTrace(System.err);
		} finally {
			db.endTransaction();
		}
		fillDatabase(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + StockContract.Stocks.TABLE_NAME);
		this.onCreate(db);
	}

	public static DatabaseHelper instance(Activity activity) {

		if (instance == null) {
			instance = new DatabaseHelper(activity.getApplicationContext());
		}
		return instance;
	}

	public static DatabaseHelper instance(Context context) {
		if (instance == null) {
			instance = new DatabaseHelper(context);
		}
		return instance;
	}

	private void fillDatabase(SQLiteDatabase db) {
		ContentValues values=new ContentValues();
		values.put(StockContract.Stocks.NAME, "Microsoft");
		values.put(StockContract.Stocks.SYMBOL, "MSFT");
		values.put(StockContract.Stocks.STARTING_PRICE, "40");
		values.put(StockContract.Stocks.LAST_TRADE, "42");
		values.put(StockContract.Stocks.DATE_STARTED, "1395069000000");
		values.put(StockContract.Stocks.LAST_DATE_CHECKED, System.currentTimeMillis());
		values.put(StockContract.Stocks.DAYS_PRICE_CHANGE, ".5");
		values.put(StockContract.Stocks.EPS, "2");
		values.put(StockContract.Stocks.EST_EARNINGS, "2");
		values.put(StockContract.Stocks.HIGH_52, "43");
		values.put(StockContract.Stocks.LOW_52, "41");
		values.put(StockContract.Stocks.VOLUME_10_DAY, "2000000");
		db.insert(StockContract.Stocks.TABLE_NAME, StockContract.Stocks.NAME, values);
		values.clear();
		values.put(StockContract.Stocks.NAME, "DOW Jones");
		values.put(StockContract.Stocks.SYMBOL, "DOW");
		values.put(StockContract.Stocks.STARTING_PRICE, "16221.16");
		values.put(StockContract.Stocks.LAST_TRADE, "16300");
		values.put(StockContract.Stocks.DATE_STARTED, "1395070000000");
		values.put(StockContract.Stocks.LAST_DATE_CHECKED, System.currentTimeMillis());
		values.put(StockContract.Stocks.DAYS_PRICE_CHANGE, ".6");
		values.put(StockContract.Stocks.EPS, "1");
		values.put(StockContract.Stocks.EST_EARNINGS, "200000000000");
		values.put(StockContract.Stocks.HIGH_52, "16400");
		values.put(StockContract.Stocks.LOW_52, "16100");
		values.put(StockContract.Stocks.VOLUME_10_DAY, "200000000");
		db.insert(StockContract.Stocks.TABLE_NAME, StockContract.Stocks.NAME, values);
		SharedPreferencesManager.getSharedPreferences(context);
		SharedPreferencesManager.addFavorite("MSFT");
		SharedPreferencesManager.addFavorite("DOW");
	}
}