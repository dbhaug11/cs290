package com.example.stocktracking;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.SimpleCursorAdapter.ViewBinder;

public class MainActivity extends ListActivity implements
		LoaderCallbacks<Cursor> {

	SimpleCursorAdapter mAdapter;
	LoaderManager loadermanager;
	CursorLoader cursorLoader;
	SharedPreferences prefs = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		prefs = SharedPreferencesManager.getSharedPreferences(this
				.getApplicationContext());

		setupListAdapter();
		setupSearchBar();
		SyncUtils.CreateSyncAccount(this);
	}

	private void setupListAdapter() {
		loadermanager = getLoaderManager();

		String[] from = { StockContract.Stocks.NAME,
				StockContract.Stocks.SYMBOL, StockContract.Stocks.LAST_TRADE,
				StockContract.Stocks.DAYS_PRICE_CHANGE };
		int[] to = { R.id.company_name, R.id.stock_symbol, R.id.stock_price,
				R.id.price_change };
		mAdapter = new SimpleCursorAdapter(this, R.layout.stock_listing_row,
				null, from, to, 0);
		loadermanager.initLoader(1, null, this);
		setListAdapter(mAdapter);
	}

	private void setupSearchBar() {
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) findViewById(R.id.search_bar);
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(new ComponentName(this,
						SearchableActivity.class)));
		searchView.setIconifiedByDefault(false);
	}

	@Override
	public void onWindowFocusChanged(boolean arg) {
		super.onWindowFocusChanged(arg);
		// TODO fix login/out button change
		if (arg) {
			if (prefs.getLong("timestamp", 0) + 7200 > System
					.currentTimeMillis()) {
				logout();
			}
			Log.e("debug", "stamp " + prefs.getLong("timestamp", 0));
			Log.e("debug", "current " + System.currentTimeMillis());
			checkforLogin();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		checkforLogin();

	}

	private void checkforLogin() {
		if (prefs.getString(OAuth.OAUTH_TOKEN, "") != "") {
			Log.e("debug", "logged in!");
			Button button = (Button) findViewById(R.id.logon);
			button.setText(getResources().getString(R.string.logout));
		}
	}

	private void logout() {

		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... arg0) {
				SharedPreferences prefs = SharedPreferencesManager
						.getSharedPreferences(getApplicationContext());
				OAuthConsumer consumer = getConsumer(prefs);
				DefaultHttpClient httpclient = new DefaultHttpClient();
				String url = Constants.REVOKE_URL;

				Log.e("debug", url);
				HttpGet request = new HttpGet(url);

				url += "&oauth_token=" + prefs.getString(OAuth.OAUTH_TOKEN, "");
				try {
					consumer.sign(request);
					Log.e("debug", request.getURI().toString());
					httpclient.execute(request);
				} catch (OAuthMessageSignerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OAuthExpectationFailedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (OAuthCommunicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return null;
			}

		}.execute();

		final Editor edit = prefs.edit();
		Log.e("debug", "removing token from prefs");
		edit.remove(OAuth.OAUTH_TOKEN);
		edit.remove(OAuth.OAUTH_TOKEN_SECRET);
		edit.commit();
		checkforLogin();
	}

	public void launchOauth(View v) {
		Button b = (Button) v;
		if (b.getText().equals(getResources().getString(R.string.logout))) {
			logout();
			b.setText(getResources().getString(R.string.logon));
			return;
		}
		startActivity(new Intent().setClass(v.getContext(),
				PrepareRequestTokenActivity.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {

		Intent intent = new Intent(this, StockDetailActivity.class);
		TextView tv = (TextView) v.findViewById(R.id.stock_symbol);
		Log.e("debugMain", tv.getText().toString());
		intent.putExtra(getResources().getString(R.string.extras_id), tv
				.getText().toString());
		startActivity(intent);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return new CursorLoader(this, StockContract.Stocks.CONTENT_URI,
				StockContract.HOME_SCREEN_PROJECTION, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor c) {
		if (mAdapter != null && c != null) {
			mAdapter.swapCursor(c);
			ViewBinder vb = new CustomViewBinder();
			mAdapter.setViewBinder(vb);
		} else {
			Log.v("cursorLoader", "OnLoadFinished, mAdapter is null");
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		if (mAdapter != null) {
			mAdapter.swapCursor(null);

		} else {
			Log.v("cursorLoader", "OnLoaderReset, mAdapter is null");
		}
	}

	class CustomViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
			if (view.getId() == R.id.price_change) {
				String temp = cursor.getString(columnIndex);

				TextView tv = (TextView) view;
				tv.setText(temp.subSequence(0,
						temp.length() > 5 ? 5 : temp.length() - 1));
				tv.setTextColor(Double.valueOf(temp) > 0 ? getResources()
						.getColor(R.color.green) : Color.RED);
				return true;
			}
			return false;
		}
	}

	private OAuthConsumer getConsumer(SharedPreferences prefs) {
		String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
		String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
		consumer.setTokenWithSecret(token, secret);
		return consumer;
	}
}