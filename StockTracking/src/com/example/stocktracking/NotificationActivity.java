package com.example.stocktracking;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class NotificationActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			NotificationFragment fragment = new NotificationFragment();
			fragment.setArguments(arguments);
			getFragmentManager().beginTransaction()
					.add(R.id.stock_detail_container, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notification_create, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void submitNotification(View v) {
		Spinner spinner = (Spinner) findViewById(R.id.limit_spinner);
		String spinnerChoice = (String) spinner.getSelectedItem();
		EditText et = (EditText) findViewById(R.id.price_setting);
		String limit=et.getText().toString();
		if(limit.equals("")||spinnerChoice.equals("")){
			Toast.makeText(getApplicationContext(), "fill out all the fields", Toast.LENGTH_SHORT).show();
			return;
		}
		SharedPreferencesManager.addNotification(spinnerChoice
				+ ":"
				+ et.getText()
				+ ":"
				+ getIntent().getStringExtra(
						getResources().getString(R.string.extras_id)));
		Toast.makeText(getApplicationContext(), "successfully added notification", Toast.LENGTH_SHORT).show();
	}

}
