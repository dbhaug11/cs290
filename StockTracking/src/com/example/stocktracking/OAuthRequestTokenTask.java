package com.example.stocktracking;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

public class OAuthRequestTokenTask extends AsyncTask<Void, Void, Void> {
	final String TAG = getClass().getName();
	private Context context;
	private OAuthProvider provider;
	private OAuthConsumer consumer;

	public OAuthRequestTokenTask(Context context, OAuthConsumer consumer,
			OAuthProvider provider) {
		this.context = context;
		this.consumer = consumer;
		this.provider = provider;
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		String err="Error trying to retrieve Oauth request token request";
		try{
			Log.i(TAG, "Retrieving request token from server");
			final String url=provider.retrieveRequestToken(consumer, "oob");
			String[] urls=url.split("oauth_");
			String url0=urls[0]+urls[1];
			
			Log.i(TAG,"Start browser with authorize URL: "+url0);
			Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url0)).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_FROM_BACKGROUND);
			context.startActivity(intent);
		} catch (Exception e){
			Log.e(TAG,err);
			e.printStackTrace();
		}
		return null;
	}

}
