package com.example.stocktracking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Stack;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class SearchableActivity extends ListActivity {
	final private String TAG = "searchActivity";
	protected static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SearchableActivity.context = this.getApplicationContext();
		Intent intent = getIntent();
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			AsyncTask<String, Void, Stock[]> task = new AsyncEtradeQuery();
			task.execute(query);

			setListAdapter(new CustomStockListAdapter(this,
					R.layout.stock_result_row, new Stock[] {}));
		}
		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.searchable, menu);
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(this, StockDetailActivity.class);
		TextView tv = (TextView) v.findViewById(R.id.stock_symbol_search);
		intent.putExtra(getResources().getString(R.string.extras_id), tv
				.getText().toString());
		startActivity(intent);
	}

	class AsyncEtradeQuery extends AsyncTask<String, Void, Stock[]> {

		@Override
		protected Stock[] doInBackground(String... arg0) {

			Stock[] stocks = null;
			try {
				SharedPreferences prefs = SharedPreferencesManager
						.getSharedPreferences(context);
				OAuthConsumer consumer = getConsumer(prefs);

				Log.e("debug", "the beginning of query");
				DefaultHttpClient httpclient = new DefaultHttpClient();
				String url = Constants.PRODUCT_LOOKUP + "?company=" + arg0[0]
						+ "&type=EQ";
				url += "&oauth_token=" + prefs.getString(OAuth.OAUTH_TOKEN, "");

				Log.e("debug", url);
				HttpGet request = new HttpGet(url);

				consumer.sign(request);
				Log.e("debug", request.getURI().toString());
				HttpResponse response = httpclient.execute(request);
				InputStream data = response.getEntity().getContent();
				stocks = parseXml(data);

			} catch (OAuthMessageSignerException e) {
				Log.e(TAG, "Signer Exception");
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				Log.e(TAG, "Expectation Failed Exception");
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				Log.e(TAG, "Communication Exception");
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				Log.e(TAG, "Client Protocol Exception");
				e.printStackTrace();
			} catch (IOException e) {
				Log.e(TAG, "IO Exception");
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				Log.e(TAG, "XmlPullParserException");
				e.printStackTrace();
			}
			if(stocks!=null){
				Log.e("debug4",""+stocks.length);
			}
			
			return stocks;

		}

		@Override
		protected void onPostExecute(Stock[] args) {
			SearchableActivity.this.setListAdapter(new CustomStockListAdapter(
					SearchableActivity.this, R.layout.stock_result_row, args));
		}

		private Stock[] parseXml(InputStream data)
				throws XmlPullParserException, IOException {
			ArrayList<Stock> stocks = null;
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(data));

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			Stack<String> stack = null;
			Stock temp = null;

			xpp.setInput(bufferedReader);
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {

				if (eventType == XmlPullParser.START_DOCUMENT) {
					stocks = new ArrayList<Stock>();
					stack = new Stack<String>();

				} else if (eventType == XmlPullParser.START_TAG) {
					if (xpp.getName().equals("Product")) {
						temp = new Stock();
					}
					stack.push(xpp.getName());
				} else if (eventType == XmlPullParser.END_TAG) {
					if (xpp.getName().equals("Product")) {
						stocks.add(temp);
					}
					stack.pop();

				} else if (eventType == XmlPullParser.TEXT) {
					if (stack.peek().equals("companyName")) {
						temp.setName(xpp.getText());
					} else if (stack.peek().equals("symbol")) {
						temp.setSymbol(xpp.getText());
					}
				}
				eventType = xpp.next();
			}

			return stocks == null ? new Stock[] {} : stocks
					.toArray(new Stock[stocks.size()]);
		}
	}

	private OAuthConsumer getConsumer(SharedPreferences prefs) {
		String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
		String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");
		Log.e("debug", token + "   " + secret);
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
		consumer.setTokenWithSecret(token, secret);
		return consumer;
	}
}
