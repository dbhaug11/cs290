package com.example.stocktracking;

import java.util.Set;
import java.util.TreeSet;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPreferencesManager {
	private static final String APP_SETTINGS="APP_SETTINGS";
	private static SharedPreferences prefs;
	private static Set<String> favorites;
	private static final String key="Preferences_Favorites";
	private static Set<String> notifications;
	private static final String notificationKey="notification_prefs";
	
	private SharedPreferencesManager(){}
	
	public static SharedPreferences getSharedPreferences(Context context) {
		if(prefs==null){
			prefs=context.getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
			favorites=prefs.getStringSet(key, new TreeSet<String>());
			notifications=prefs.getStringSet(notificationKey, new TreeSet<String>());

		}
		return prefs;	
	}
	
	public static boolean addFavorite(String favoriteSymbol){
		if(favorites!=null){
			favorites.add(favoriteSymbol);
			Log.e("debugPrefs",favoriteSymbol+" added");
			updateFavoritesPrefs();
			return true;
		}
		return false;
	}
	
	public static boolean removeFavorite(String favoriteSymbol){
		if(favorites!=null){
			favorites.remove(favoriteSymbol);
			Log.e("debugPrefs",favoriteSymbol+" removed");
			updateFavoritesPrefs();
			return true;
		}
		return false;
	}
	
	private static void updateFavoritesPrefs(){
		if (prefs!=null){
			Editor edit=prefs.edit();
			edit.remove(key);
			edit.putStringSet(key, favorites);
			edit.commit();
		}
	}
	
	public static boolean addNotification(String notification){
		if(notifications!=null){
			notifications.add(notification);
			Log.e("debugPrefs", notification+" added");
			updateNotificationPrefs();
			return true;
		}
		return false;
	}
	
	

	public static boolean removeNotification(String notification){
		if(notifications!=null){
			notifications.remove(notification);
			Log.e("debugPrefs", notification+" removed");
			updateNotificationPrefs();
			return true;
		}
		return false;
	}
	
	private static void updateNotificationPrefs() {
		if (prefs!=null){
			Editor edit=prefs.edit();
			edit.remove(notificationKey);
			edit.putStringSet(notificationKey, notifications);
			edit.commit();
		}
	}
}
