package com.example.stocktracking;


// rkg
// Nice encapsulation of stock!
public class Stock {
	private String symbol;
	private String companyName;
	private String eps;
	private String estEarnings;
	private String high52;
	private String low52;
	private String lastTrade;
	private String volume10Day;
	private Long time;
	private String changeSinceLastClose;
	private Long dateStartedTracking;
	private String startingPrice;

	public String getStartingPrice() {
		return startingPrice;
	}

	public void setStartingPrice(String startingPrice) {
		this.startingPrice = startingPrice;
	}

	protected Long getDateStartedTracking() {
		return dateStartedTracking;
	}

	protected void setDateStartedTracking(Long dateStartedTracking) {
		this.dateStartedTracking = dateStartedTracking;
	}

	public String getChangeSinceLastClose() {
		return changeSinceLastClose;
	}

	public void setChangeSinceLastClose(String changeSinceLastClose) {
		this.changeSinceLastClose = changeSinceLastClose;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getEps() {
		return eps;
	}

	public void setEps(String eps) {
		this.eps = eps;
	}

	public String getEstEarnings() {
		return estEarnings;
	}

	public void setEstEarnings(String estEarnings) {
		this.estEarnings = estEarnings;
	}

	public String getHigh52() {
		return high52;
	}

	public void setHigh52(String high52) {
		this.high52 = high52;
	}

	public String getLow52() {
		return low52;
	}

	public void setLow52(String low52) {
		this.low52 = low52;
	}

	public String getLastTrade() {
		return lastTrade;
	}

	public void setLastTrade(String lastTrade) {
		this.lastTrade = lastTrade;
	}

	public String getVolume10Day() {
		return volume10Day;
	}

	public void setVolume10Day(String volume10Day) {
		this.volume10Day = volume10Day;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return companyName;
	}

	public void setName(String name) {
		this.companyName = name;
	}
}
