package com.example.stocktracking;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

public class StockContentProvider extends ContentProvider {
	
	private DatabaseHelper db;
	private String table=StockContract.Stocks.TABLE_NAME;

	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		return db.getWritableDatabase().delete(table, arg1, arg2);
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri arg0, ContentValues arg1) {
		long id = db.getReadableDatabase().insert(table, null, arg1);
		return ContentUris.withAppendedId(StockContract.Stocks.CONTENT_URI,
				id);
	}

	@Override
	public boolean onCreate() {
		db=DatabaseHelper.instance(getContext().getApplicationContext());
		db.getReadableDatabase();
		return true;
	}

	@Override
	public Cursor query(Uri arg0, String[] arg1, String arg2, String[] arg3,
			String arg4) {
		
		return db.getReadableDatabase().query(table, arg1, arg2, arg3, null, null, null);
	}

	@Override
	public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
		return db.getWritableDatabase().update(table, arg1, arg2, arg3);
	}

}
