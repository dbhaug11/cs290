package com.example.stocktracking;

import android.net.Uri;
import android.provider.BaseColumns;

public class StockContract {
	private StockContract() {

	}

	public static final String AUTHORITY = "com.example.stocktracking.stockProvider";
	public static final String[] HOME_SCREEN_PROJECTION = { Stocks._ID,
			Stocks.NAME, Stocks.SYMBOL, Stocks.LAST_TRADE,
			Stocks.DAYS_PRICE_CHANGE };
	public static final String[] SYNC_PROJECTION = { Stocks._ID, Stocks.SYMBOL,
			Stocks.DATE_STARTED, Stocks.STARTING_PRICE };
	public static final String[] DETAILS_PROJECTION = { Stocks._ID,
			Stocks.NAME, Stocks.SYMBOL, Stocks.STARTING_PRICE,
			Stocks.DATE_STARTED, Stocks.LAST_DATE_CHECKED,
			Stocks.DAYS_PRICE_CHANGE, Stocks.EPS, Stocks.EST_EARNINGS,
			Stocks.HIGH_52, Stocks.LOW_52, Stocks.LAST_TRADE,
			Stocks.VOLUME_10_DAY };

	public static abstract class Stocks implements BaseColumns {
		public static final String TABLE_NAME = "stocks";
		public static final String NAME = "name";
		public static final String SYMBOL = "symbol";
		public static final String STARTING_PRICE = "startingPrice";
		public static final String DATE_STARTED = "dateStarted";
		public static final String LAST_DATE_CHECKED = "lastDateChecked";
		public static final String DAYS_PRICE_CHANGE = "daysPriceChange";
		public static final String EPS = "eps";
		public static final String EST_EARNINGS = "estEarnings";
		public static final String HIGH_52 = "high52";
		public static final String LOW_52 = "low52";
		public static final String LAST_TRADE = "lastTrade";
		public static final String VOLUME_10_DAY = "volume10Day";
		public static final Uri CONTENT_URI = Uri.parse("content://"
				+ AUTHORITY + "/stocks");
	}
}
