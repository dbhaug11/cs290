package com.example.stocktracking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class StockDetailActivity extends Activity {

	private SharedPreferences prefs;
	private boolean local;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stock_detail);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		String key = getIntent().getStringExtra(
				(getResources().getString(R.string.extras_id)));
		prefs = SharedPreferencesManager
				.getSharedPreferences(getApplicationContext());
		Set<String> favoriteSet = prefs.getStringSet(
				getResources().getString(R.string.favorites), null);
		if (favoriteSet != null) {
			Log.e("debugDetails", "favorites not empty");
			String[] favorites = (String[]) favoriteSet
					.toArray(new String[favoriteSet.size()]);
			for (String s : favorites) {
				Log.e("debugDetails", "is " + s + " = " + key);
				if (s.equals(key)) {
					AsyncTask<String, Integer, Cursor> task = new asyncDatabaseTask();
					task.execute(key);
					local=true;
					return;
				}
			}
		}
		AsyncTask<String, Void, Stock[]> task = new asyncEtradeQuote();
		task.execute(key);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.favorite_option:
			if (item.isChecked()) {
				Log.e("debugDetails","unchecking");
				item.setChecked(false);
				item.setTitle(R.string.check_box);
				SyncUtils.TriggerRefresh();
				return SharedPreferencesManager
						.removeFavorite(((TextView) findViewById(R.id.company_symbol_details))
								.getText().toString());
			} else {
				Log.e("debugDetails","checking");
				item.setChecked(true);
				item.setTitle(R.string.uncheck_box);
				SyncUtils.TriggerRefresh();
				return SharedPreferencesManager
						.addFavorite(((TextView) findViewById(R.id.company_symbol_details))
								.getText().toString());
			}
		case R.id.add_limit_notification:
			String symbol = ((TextView) findViewById(R.id.company_symbol_details))
					.getText().toString();
			Intent intent=new Intent(this,NotificationActivity.class);
			intent.putExtra(getResources().getString(R.string.extras_id), symbol);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stock_detail, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(local){
			MenuItem item=(MenuItem) menu.findItem(R.id.favorite_option);
			item.setChecked(true);
			item.setTitle(R.string.uncheck_box);
		}
		return super.onPrepareOptionsMenu(menu);
	}
	
	public void submitNotification(View v) {
		
		Spinner spinner = (Spinner) findViewById(
				R.id.limit_spinner);
		String spinnerChoice = (String) spinner.getSelectedItem();
		EditText et = (EditText) findViewById(R.id.price_setting);
		String limit=et.getText().toString();
		if(limit.equals("")||spinnerChoice.equals("")){
			Toast.makeText(getApplicationContext(), "please fill out all the fields", Toast.LENGTH_SHORT).show();
			return;
		}
		SharedPreferencesManager.addNotification(spinnerChoice
				+ ":"
				+ limit
				+ ":"
				+ ((TextView) findViewById(
						R.id.company_symbol_details)).getText());
		Toast.makeText(getApplicationContext(), "successfully added notification", Toast.LENGTH_SHORT).show();
		}

	private void fillViews(Stock stock) {
		Calendar cal=Calendar.getInstance();
		
		TextView tv = (TextView) findViewById(R.id.company_name_details);
		tv.setText(stock.getName());

		tv = (TextView) findViewById(R.id.company_symbol_details);
		tv.setText(stock.getSymbol());

		tv = (TextView) findViewById(R.id.stock_price_details);
		tv.setText(stock.getLastTrade());

		tv = (TextView) findViewById(R.id.stock_price_change_details);
		String temp = stock.getChangeSinceLastClose();
		tv.setTextColor(Double.valueOf(temp) > 0 ? getResources().getColor(R.color.green) : Color.RED);

		tv.setText(temp.subSequence(0, temp.length() > 4 ? 4
				: temp.length() - 1));
		
		tv = (TextView) findViewById(R.id.date_last_checked);
		cal.setTimeInMillis(stock.getTime());
		Date date=cal.getTime();
		tv.setText(date.toString());
		
		tv = (TextView) findViewById(R.id.eps_details);
		tv.setText(stock.getEps());
		
		tv = (TextView) findViewById(R.id.earnings_details);
		tv.setText(stock.getEstEarnings());
		
		tv = (TextView) findViewById(R.id.high52_details);
		tv.setText(stock.getHigh52());
		
		tv = (TextView) findViewById(R.id.low52_details);
		tv.setText(stock.getLow52());
		
		tv = (TextView) findViewById(R.id.volume_details);
		tv.setText(stock.getVolume10Day());
		
		if (stock.getDateStartedTracking() != null) {
			tv = (TextView) findViewById(R.id.date_started_tracking);
			cal.setTimeInMillis(stock.getDateStartedTracking());
			date=cal.getTime();
			tv.setText(date.toString());
			
			tv = (TextView) findViewById(R.id.total_gain);
			Double totalGain = Double.valueOf(stock.getLastTrade())
					- Double.valueOf(stock.getStartingPrice());
			tv.setTextColor(totalGain > 0 ? getResources().getColor(
					R.color.green) : Color.RED);
			String total=totalGain+"";
			
			tv.setText(total.subSequence(0, total.length() > 4 ? 4
					: total.length() - 1));
		}
	}

	class asyncDatabaseTask extends AsyncTask<String, Integer, Cursor> {

		@Override
		protected Cursor doInBackground(String... params) {
			Cursor c = getContentResolver().query(
					StockContract.Stocks.CONTENT_URI,
					StockContract.DETAILS_PROJECTION, StockContract.Stocks.SYMBOL+"=?", params, null);
			c.moveToFirst();
			return c;
		}

		@Override
		protected void onPostExecute(Cursor c) {
			Stock stock = new Stock();
			if (c != null) {
				stock.setChangeSinceLastClose(c.getString(c
						.getColumnIndex(StockContract.Stocks.DAYS_PRICE_CHANGE)));
				stock.setEps(c.getString(c
						.getColumnIndex(StockContract.Stocks.EPS)));
				stock.setEstEarnings(c.getString(c
						.getColumnIndex(StockContract.Stocks.EST_EARNINGS)));
				stock.setHigh52(c.getString(c
						.getColumnIndex(StockContract.Stocks.HIGH_52)));
				stock.setLastTrade(c.getString(c
						.getColumnIndex(StockContract.Stocks.LAST_TRADE)));
				stock.setLow52(c.getString(c
						.getColumnIndex(StockContract.Stocks.LOW_52)));
				stock.setName(c.getString(c
						.getColumnIndex(StockContract.Stocks.NAME)));
				stock.setSymbol(c.getString(c
						.getColumnIndex(StockContract.Stocks.SYMBOL)));
				stock.setTime(c.getLong(c
						.getColumnIndex(StockContract.Stocks.LAST_DATE_CHECKED)) * 1000);
				stock.setDateStartedTracking(c.getLong(c
						.getColumnIndex(StockContract.Stocks.DATE_STARTED)));
				stock.setStartingPrice(c.getString(c
						.getColumnIndex(StockContract.Stocks.STARTING_PRICE)));
			}
			fillViews(stock);
			c.close();

		}
	}

	class asyncEtradeQuote extends AsyncTask<String, Void, Stock[]> {

		@Override
		protected Stock[] doInBackground(String... params) {
			SharedPreferences prefs = SharedPreferencesManager
					.getSharedPreferences(getApplicationContext());

			OAuthConsumer consumer = getConsumer(prefs);

			DefaultHttpClient httpclient = new DefaultHttpClient();
			String url = Constants.QUOTE_URL + params[0] + "?detailFlag=ALL";

			Log.e("debug", url);
			HttpGet request = new HttpGet(url);

			url += "&oauth_token=" + prefs.getString(OAuth.OAUTH_TOKEN, "");
			Stock[] ret = new Stock[]{};

			try {
				consumer.sign(request);
				Log.e("debug", request.getURI().toString());
				HttpResponse response = httpclient.execute(request);
				InputStream data = response.getEntity().getContent();

				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(data));
				
				ret=SyncUtils.parseXml(bufferedReader);

				/*XmlPullParserFactory factory = XmlPullParserFactory
						.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();

				Stack<String> stack = null;
				Stock temp = null;

				xpp.setInput(bufferedReader);
				int eventType = xpp.getEventType();
				while (eventType != XmlPullParser.END_DOCUMENT) {

					if (eventType == XmlPullParser.START_DOCUMENT) {
						stack = new Stack<String>();
						stocksList = new ArrayList<Stock>();

					} else if (eventType == XmlPullParser.START_TAG) {
						if (xpp.getName().equals("QuoteData")) {

							temp = new Stock();
						}
						stack.push(xpp.getName());
					} else if (eventType == XmlPullParser.END_TAG) {
						if (xpp.getName().equals("QuoteData")) {
							stocksList.add(temp);
						}
						stack.pop();

					} else if (eventType == XmlPullParser.TEXT) {
						if (stack.peek().equals("companyName")) {
							temp.setName(xpp.getText());
						} else if (stack.peek().equals("symbol")) {
							temp.setSymbol(xpp.getText());
						} else if (stack.peek().equals("dateTime")) {
							SimpleDateFormat dateFormat = new SimpleDateFormat(
									"HH:mm:ss ZZZ MM-dd-yyyy");
							temp.setTime(dateFormat.parse(xpp.getText())
									.getTime());
						} else if (stack.peek().equals("eps")) {
							temp.setEps(xpp.getText());
						} else if (stack.peek().equals("estEarnings")) {
							temp.setEstEarnings(xpp.getText());
						} else if (stack.peek().equals("high52")) {
							temp.setHigh52(xpp.getText());
						} else if (stack.peek().equals("low52")) {
							temp.setLow52(xpp.getText());
						} else if (stack.peek().equals("lastTrade")) {
							temp.setLastTrade(xpp.getText());
						} else if (stack.peek().equals("volume10Day")) {
							temp.setVolume10Day(xpp.getText());
						} else if (stack.peek().equals("chgClose")) {
							temp.setChangeSinceLastClose(xpp.getText());
						}
					}
					eventType = xpp.next();
				}
				
			*/

			} catch (OAuthMessageSignerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return ret;
		}

		@Override
		protected void onPostExecute(Stock[] arr) {
			fillViews(arr[0]);
		}

	}

	private OAuthConsumer getConsumer(SharedPreferences prefs) {
		String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
		String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
		consumer.setTokenWithSecret(token, secret);
		return consumer;
	}
}
