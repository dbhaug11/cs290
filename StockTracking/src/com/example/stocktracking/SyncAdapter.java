package com.example.stocktracking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.accounts.Account;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

	ContentResolver mContentResolver;

	public SyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		Log.e("debug", "adapter Constructed");
		mContentResolver = context.getContentResolver();
	}

	public SyncAdapter(Context context, boolean autoInitialize,
			boolean allowParallelSyncs) {
		super(context, autoInitialize, allowParallelSyncs);
		mContentResolver = context.getContentResolver();
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {
		// TODO Auto-generated method stub

		SharedPreferences prefs = SharedPreferencesManager
				.getSharedPreferences(getContext());
		OAuthConsumer consumer = getConsumer(prefs);
		DefaultHttpClient httpclient = new DefaultHttpClient();

		Set<String> favoriteSet = prefs.getStringSet(getContext()
				.getResources().getString(R.string.favorites), null);
		String[] favorites;

// rkg
// Consider approach of Provider (oauth vernacular) that presents
// (or creates) its authorize, requst_token, revoke urls
// provider.generateAuthorizeUrl(token)
// Provider hide all details of arg names and e.g. presence of
// detailFlag=ALL 
		String url = Constants.QUOTE_URL;

		if (favoriteSet != null) {
			favorites = favoriteSet.toArray(new String[favoriteSet.size()]);
			for (String s : favorites) {
				url += "," + s;
				Log.e("debugSync", "Favorite: " + s);
			}
		}

		url += "?detailFlag=ALL";
		url += "&oauth_token=" + prefs.getString(OAuth.OAUTH_TOKEN, "");

		HttpGet request = new HttpGet(url);
		Stock[] ret = new Stock[] {};
		try {

			consumer.sign(request);
			HttpResponse response = httpclient.execute(request);
			InputStream data = response.getEntity().getContent();

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(data));

			ret = SyncUtils.parseXml(bufferedReader);

// rkg
// ???????????????????????
// Get rid of this code
			/*
			 * XmlPullParserFactory factory =
			 * XmlPullParserFactory.newInstance();
			 * factory.setNamespaceAware(true); XmlPullParser xpp =
			 * factory.newPullParser(); xpp.setInput(bufferedReader); int
			 * eventType = xpp.getEventType();
			 * 
			 * Stack<String> stack = null; Stock temp = null;
			 * 
			 * while (eventType != XmlPullParser.END_DOCUMENT) {
			 * 
			 * if (eventType == XmlPullParser.START_DOCUMENT) { stack = new
			 * Stack<String>(); stocksList = new ArrayList<Stock>();
			 * 
			 * } else if (eventType == XmlPullParser.START_TAG) { if
			 * (xpp.getName().equals("QuoteData")) {
			 * 
			 * temp = new Stock(); } stack.push(xpp.getName()); } else if
			 * (eventType == XmlPullParser.END_TAG) { if
			 * (xpp.getName().equals("QuoteData")) { stocksList.add(temp); }
			 * stack.pop();
			 * 
			 * } else if (eventType == XmlPullParser.TEXT) { if
			 * (stack.peek().equals("companyName")) {
			 * temp.setName(xpp.getText()); } else if
			 * (stack.peek().equals("symbol")) { temp.setSymbol(xpp.getText());
			 * } else if (stack.peek().equals("dateTime")) { SimpleDateFormat
			 * dateFormat = new SimpleDateFormat( "HH:mm:ss ZZZ MM-dd-yyyy");
			 * temp.setTime(dateFormat.parse(xpp.getText()).getTime()); } else
			 * if (stack.peek().equals("eps")) { temp.setEps(xpp.getText()); }
			 * else if (stack.peek().equals("estEarnings")) {
			 * temp.setEstEarnings(xpp.getText()); } else if
			 * (stack.peek().equals("high52")) { temp.setHigh52(xpp.getText());
			 * } else if (stack.peek().equals("low52")) {
			 * temp.setLow52(xpp.getText()); } else if
			 * (stack.peek().equals("lastTrade")) {
			 * temp.setLastTrade(xpp.getText());
			 * temp.setStartingPrice(xpp.getText()); } else if
			 * (stack.peek().equals("volume10Day")) {
			 * temp.setVolume10Day(xpp.getText()); } else if
			 * (stack.peek().equals("chgClose")) {
			 * temp.setChangeSinceLastClose(xpp.getText()); } }
			 * 
			 * eventType = xpp.next(); }
			 */
			updateClientData(provider, ret);

		} catch (OAuthMessageSignerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OAuthExpectationFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OAuthCommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void updateClientData(ContentProviderClient provider, Stock[] stocks)
			throws RemoteException {

// rkg
// Why all these multiple representations of your list of stocks?
// 
		ArrayList<Stock> stock = new ArrayList<Stock>();
		for (int i = 0; i < stocks.length; i++) {
			stock.add(stocks[i]);
		}
// rkg
// You process stock list twice
// Why can't you check for limits within update loop below?
//
		checkForNotifications(stocks);
		Cursor c = provider.query(StockContract.Stocks.CONTENT_URI,
				StockContract.SYNC_PROJECTION, null, null, null);
		c.moveToFirst();
		for (int i = 0; i < c.getCount(); i++) {
			String symbol = c.getString(c
					.getColumnIndex(StockContract.Stocks.SYMBOL));
			int index = checkForExistence(symbol, stock);
			if (index > -1) {
				stock.get(index)
						.setDateStartedTracking(
								Long.valueOf(c.getString(c
										.getColumnIndex(StockContract.Stocks.DATE_STARTED))));
				stock.get(index)
						.setStartingPrice(
								c.getString(c
										.getColumnIndex(StockContract.Stocks.STARTING_PRICE)));
				provider.update(StockContract.Stocks.CONTENT_URI,
						SyncUtils.StockToContentValues(stock.get(index)),
						StockContract.Stocks.SYMBOL + "=?",
						new String[] { stock.get(index).getSymbol() });
				Log.e("debugSync", "updating " + stock.get(index).getSymbol());

				stock.remove(index);
			} else if (index == -1) {
				provider.delete(StockContract.Stocks.CONTENT_URI,
						StockContract.Stocks.SYMBOL + "=?",
						new String[] { c.getString(c
								.getColumnIndex(StockContract.Stocks.SYMBOL)) });
				Log.e("debugSync", "deleting " + symbol);
			}

			if (c.getCount() > i + 1) {
				c.moveToNext();
			}
		}
		for (int i = 0; i < stock.size(); i++) {
			stock.get(i).setDateStartedTracking(stock.get(i).getTime());
			provider.insert(StockContract.Stocks.CONTENT_URI,
					SyncUtils.StockToContentValues(stock.get(i)));
			Log.e("debugSync", "inserting " + stock.get(i).getSymbol());
		}
	}

	private int checkForExistence(String symbol, ArrayList<Stock> stock) {
		for (int i = 0; i < stock.size(); i++) {
			if (stock.get(i).getSymbol().equals(symbol)) {
				return i;
			}
		}
		return -1;
	}

	private OAuthConsumer getConsumer(SharedPreferences prefs) {
		String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
		String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
		consumer.setTokenWithSecret(token, secret);
		return consumer;
	}

	private void checkForNotifications(Stock[] stocks) {
		SharedPreferences prefs = SharedPreferencesManager
				.getSharedPreferences(getContext());
		Set<String> notifications = prefs.getStringSet(getContext()
				.getResources().getString(R.string.notification_key),
				new TreeSet<String>());
		
		
		ArrayList<Stock> toCheck = new ArrayList<Stock>();
		ArrayList<Stock> toNotify = new ArrayList<Stock>();
		if (notifications.size() != 0) {
			String[] notificationArray = notifications
					.toArray(new String[notifications.size()]);
			for (int i = 0; i < notificationArray.length; i++) {
				String[] temp = notificationArray[i].split(":");
				for (int j = 0; j < temp.length; j++) {
					Log.e("debugNotifications", temp[j]);
				}
				for (int j = 0; j < stocks.length; j++) {
					if (temp[2].equals(stocks[j].getSymbol())) {
						toCheck.add(stocks[j]);
						Log.e("debugNotifications", "checking "+temp[2]);
					}
				}
				for (int j = 0; j < toCheck.size(); j++) {
					Double limit = Double.valueOf(temp[1]);
					Double value = Double
							.valueOf(toCheck.get(j).getLastTrade());
					if (temp[0].equals("Above")) {
						if (value > limit) {
							toNotify.add(toCheck.get(j));
							SharedPreferencesManager.removeNotification(notificationArray[i]);
						}
					} else {

					}
				}
			}
		} else if (notifications.size() == 0) {
			return;
		}
		buildNotification(toNotify.toArray(new Stock[toNotify.size()]));

	}

	private void buildNotification(Stock[] stocks) {
		if (stocks.length == 0) {
			return;
		}
		NotificationManager nm = (NotificationManager) getContext()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		String contentTitle = stocks.length + " stock hit set limit";
		String contentText = "";
		for (int i = 0; i < stocks.length; i++) {
			contentText += stocks[i].getSymbol();
			if (i < stocks.length - 3) {
				contentText += ", ";
			} else if (i < stocks.length - 2) {
				contentText += ", and";
			}
		}
		Intent intent = new Intent(getContext(), StockDetailActivity.class);
		intent.putExtra(getContext().getResources().getString(R.string.extras_id), stocks[0].getSymbol());
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(getContext());
		stackBuilder.addParentStack(StockDetailActivity.class);

		stackBuilder.addNextIntent(intent);
		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		Notification noti = new NotificationCompat.Builder(getContext())
				.setContentTitle(contentTitle).setContentText(contentText)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pendingIntent).build();
		nm.notify(0, noti);

	}

}
