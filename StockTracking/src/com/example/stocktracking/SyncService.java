package com.example.stocktracking;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class SyncService extends Service {
	private static final Object sAdapterLock = new Object();
	private static SyncAdapter sSyncAdapter = null;

	public void onCreate() {
		Log.e("debug", "in service");
		synchronized (sAdapterLock) {
			if (sSyncAdapter == null) {
				sSyncAdapter = new SyncAdapter(getApplicationContext(), true);
			}
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return sSyncAdapter.getSyncAdapterBinder();
	}

}
