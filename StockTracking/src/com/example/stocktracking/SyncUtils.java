/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// rkg
// There is a lot of code herein that you do not use.
// Unused code degrades readability. And, hence, gradability.


package com.example.stocktracking;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Stack;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import oauth.signpost.OAuth;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;


// rkg
// We DO NOT name methods with upper-case!!!!!! ...even when borrowed.
/**
 * Static helper methods for working with the sync framework.
 */
public class SyncUtils {
	private static final long SYNC_FREQUENCY = 120;
	private static final String CONTENT_AUTHORITY = StockContract.AUTHORITY;
	private static final String PREF_SETUP_COMPLETE = "setup_complete";

	/**
	 * Create an entry for this application in the system account list, if it
	 * isn't already there.
	 * 
	 * @param context
	 *            Context
	 */
	public static void CreateSyncAccount(Context context) {
		boolean newAccount = false;
		boolean setupComplete = PreferenceManager.getDefaultSharedPreferences(
				context).getBoolean(PREF_SETUP_COMPLETE, false);

		// Create account, if it's missing. (Either first run, or user has
		// deleted account.)
		Account account = GenericAccountService.GetAccount();
		SharedPreferences prefs = SharedPreferencesManager
				.getSharedPreferences(context);

		Log.e("debug", account.type);
		AccountManager accountManager = (AccountManager) context
				.getSystemService(Context.ACCOUNT_SERVICE);
		accountManager.setAuthToken(account, AccountManager.KEY_AUTHTOKEN,
				prefs.getString(OAuth.OAUTH_TOKEN, ""));

		Account[] existingAccounts = accountManager
				.getAccountsByType(account.type);
		if (existingAccounts.length == 0) {
			if (accountManager.addAccountExplicitly(account, null, null)) {
				ContentResolver.setIsSyncable(account, CONTENT_AUTHORITY, 1);
				ContentResolver.setSyncAutomatically(account,
						CONTENT_AUTHORITY, true);
				ContentResolver.addPeriodicSync(account, CONTENT_AUTHORITY,
						new Bundle(), SYNC_FREQUENCY);
			}
		}

		else if (accountManager.addAccountExplicitly(account, null, null)) {
			// Inform the system that this account supports sync
			ContentResolver.setIsSyncable(account, CONTENT_AUTHORITY, 1);
			// Inform the system that this account is eligible for auto sync
			// when the network is up
			ContentResolver.setSyncAutomatically(account, CONTENT_AUTHORITY,
					true);
			// Recommend a schedule for automatic synchronization. The system
			// may modify this based
			// on other scheduled syncs and network utilization.
			ContentResolver.addPeriodicSync(account, CONTENT_AUTHORITY,
					new Bundle(), SYNC_FREQUENCY);
			newAccount = true;
		}

		// Schedule an initial sync if we detect problems with either our
		// account or our local
		// data has been deleted. (Note that it's possible to clear app data
		// WITHOUT affecting
		// the account list, so wee need to check both.)
		if (newAccount || !setupComplete) {
			TriggerRefresh();
			PreferenceManager.getDefaultSharedPreferences(context).edit()
					.putBoolean(PREF_SETUP_COMPLETE, true).commit();
		}
	}

	/**
	 * Helper method to trigger an immediate sync ("refresh").
	 * 
	 * <p>
	 * This should only be used when we need to preempt the normal sync
	 * schedule. Typically, this means the user has pressed the "refresh"
	 * button.
	 * 
	 * Note that SYNC_EXTRAS_MANUAL will cause an immediate sync, without any
	 * optimization to preserve battery life. If you know new data is available
	 * (perhaps via a GCM notification), but the user is not actively waiting
	 * for that data, you should omit this flag; this will give the OS
	 * additional freedom in scheduling your sync request.
	 */
	public static void TriggerRefresh() {
		Bundle b = new Bundle();
		// Disable sync backoff and ignore sync preferences. In other
		// words...perform sync NOW!
		b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
		ContentResolver.requestSync(GenericAccountService.GetAccount(), // Sync
																		// account
				CONTENT_AUTHORITY, // Content authority
				b); // Extras
	}

	public static ContentValues StockToContentValues(Stock stock) {
		ContentValues values = new ContentValues();
		values.put(StockContract.Stocks.NAME, stock.getName());
		values.put(StockContract.Stocks.SYMBOL, stock.getSymbol());
		values.put(StockContract.Stocks.STARTING_PRICE,
				stock.getStartingPrice());
		values.put(StockContract.Stocks.LAST_TRADE, stock.getLastTrade());
		values.put(StockContract.Stocks.DATE_STARTED, stock.getDateStartedTracking());
		values.put(StockContract.Stocks.LAST_DATE_CHECKED, stock.getTime());
		values.put(StockContract.Stocks.DAYS_PRICE_CHANGE,
				stock.getChangeSinceLastClose());
		values.put(StockContract.Stocks.EPS, stock.getEps());
		values.put(StockContract.Stocks.EST_EARNINGS, stock.getEstEarnings());
		values.put(StockContract.Stocks.HIGH_52, stock.getHigh52());
		values.put(StockContract.Stocks.LOW_52, stock.getLow52());
		values.put(StockContract.Stocks.VOLUME_10_DAY, stock.getVolume10Day());
		return values;
	}

// rkg
// If you return ArrayList here you avoid data conversion downstream
//
	public static Stock[] parseXml(BufferedReader reader) {
		ArrayList<Stock> stocksList = null;
		XmlPullParserFactory factory;
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();

			Stack<String> stack = null;
			Stock temp = null;

			xpp.setInput(reader);
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {


// rkg 
// All string literals below should be defined as constants

				if (eventType == XmlPullParser.START_DOCUMENT) {
					stack = new Stack<String>();
					stocksList = new ArrayList<Stock>();

				} else if (eventType == XmlPullParser.START_TAG) {
					if (xpp.getName().equals("QuoteData")) {

						temp = new Stock();
					}
					stack.push(xpp.getName());
				} else if (eventType == XmlPullParser.END_TAG) {
					if (xpp.getName().equals("QuoteData")) {
						stocksList.add(temp);
					}

// rkg
// The cleanest (and most common) approach for this type of parsing
// Is to maintain collected state (between begin and end tag) on stack
// then on end tag apply semantics to current tag. IOW, you have a tag 
// handler that looks like command pattern (http://www.codeproject.com/Articles/15207/Design-Patterns-Command-Pattern)
					stack.pop();

				} else if (eventType == XmlPullParser.TEXT) {
					if (stack.peek().equals("companyName")) {
						temp.setName(xpp.getText());
					} else if (stack.peek().equals("symbol")) {
						temp.setSymbol(xpp.getText());
					} else if (stack.peek().equals("dateTime")) {
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"HH:mm:ss ZZZ MM-dd-yyyy");
						temp.setTime(dateFormat.parse(xpp.getText()).getTime());
					} else if (stack.peek().equals("eps")) {
						temp.setEps(xpp.getText());
					} else if (stack.peek().equals("estEarnings")) {
						temp.setEstEarnings(xpp.getText());
					} else if (stack.peek().equals("high52")) {
						temp.setHigh52(xpp.getText());
					} else if (stack.peek().equals("low52")) {
						temp.setLow52(xpp.getText());
					} else if (stack.peek().equals("lastTrade")) {
						temp.setLastTrade(xpp.getText());
						temp.setStartingPrice(xpp.getText());
					} else if (stack.peek().equals("volume10Day")) {
						temp.setVolume10Day(xpp.getText());
					} else if (stack.peek().equals("chgClose")) {
						temp.setChangeSinceLastClose(xpp.getText());
					}
				}
				eventType = xpp.next();
			}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
// rkg
// Yet another data conversion...
		return stocksList.toArray(new Stock[stocksList.size()]);

	}
}
