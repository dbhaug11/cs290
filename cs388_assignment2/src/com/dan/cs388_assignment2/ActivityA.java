package com.dan.cs388_assignment2;

import android.os.Bundle;
import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class ActivityA extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity, menu);
		return true;
	}
	
	public void toC(View view){
		TaskStackBuilder tsb=TaskStackBuilder.create(this);
		tsb.addNextIntentWithParentStack(new Intent(this,ActivityB.class));
		tsb.addNextIntent(new Intent(this,ActivityC.class));
		tsb.startActivities();
	}
}
